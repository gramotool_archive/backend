from django.contrib.sitemaps import Sitemap

from gramotool.iaccounts.models.accounts import InstagramAccount


class StaticSitemap(Sitemap):
    priority = 0.8
    changefreq = 'weekly'
    protocol = "https"

    def items(self):
        return ['/story/',
                '/avatar/',
                '/highlight/',
                '/saver/',
                '/nakrutka/',
                '/',
                ]

    def location(self, item):
        return item


class InstagramAccountSitemap(Sitemap):
    priority = 0.5
    changefreq = 'weekly'
    limit = 25000

    def items(self):
        return InstagramAccount.objects.only('updated_at', 'username')

    def lastmod(self, obj):
        return obj.updated_at

    def location(self, obj):
        return '/profile/{0}'.format(obj.username)
