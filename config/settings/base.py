import environ

ROOT_DIR = environ.Path(__file__) - 2  # (app/config/settings.py - 2 = app/)
APPS_DIR = ROOT_DIR.path('gramotool')
env = environ.Env()

# GENERAL
# ------------------------------------------------------------------------------
SESSION_SAVE_EVERY_REQUEST = True
SITE_ID = 1
CORS_ORIGIN_ALLOW_ALL = True

# Internationalization
# ------------------------------------------------------------------------------
LANGUAGE_CODE = 'ru-RU'
TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = False
USE_TZ = False
DATE_FORMAT = 'd E Y'
DATETIME_FORMAT = 'd E Y, H:i'

# Instagram account providers
# ------------------------------------------------------------------------------
PROVIDER_WORKING_TIME = 4
MIN_ACTIVE_PROVIDERS_COUNT = 6

# DATABASES
# ------------------------------------------------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env.str('POSTGRES_DB'),
        'USER': env.str('POSTGRES_USER'),
        'PASSWORD': env.str('POSTGRES_PASSWORD'),
        'HOST': env.str('POSTGRES_HOST'),
        'PORT': env.str('POSTGRES_PORT'),
    }
}

# URLS
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sitemaps',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'rest_framework',
    'rest_framework.authtoken',
    'drf_yasg',
    'django_rq',
    'corsheaders',
    'imagekit',
    'django_prometheus',
]

LOCAL_APPS = [
    'gramotool.users.apps.UsersConfig',
    'gramotool.tools.apps.ToolsConfig',
    'gramotool.iaccounts.apps.IaccountsConfig',
    'gramotool.compilations.apps.CompilationsConfig',
    'gramotool.articles.apps.ArticlesConfig',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# AUTHENTICATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        }
    },
]
LOGIN_URL = '/user/login/'
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False

# MIDDLEWARE
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
]

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': str(APPS_DIR.path('templates')),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# ADMIN
# ------------------------------------------------------------------------------
ADMIN_URL = 'django-admin/'

# Django REST
# ------------------------------------------------------------------------------
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
}

# CACHES
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env.str('REDIS_CACHE_URL'),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "IGNORE_EXCEPTIONS": True,
        },
    },
    'db_cache': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'cache_table',
    }
}

# RQ
# ------------------------------------------------------------------------------
RQ_QUEUES = {
    'default': {
        'URL': env.str('REDIS_RQ_URL'),
        'DEFAULT_TIMEOUT': 720,
    },
    'high': {
        'URL': env.str('REDIS_RQ_URL'),
        'DEFAULT_TIMEOUT': 360,
    },
    'schedule': {
        'URL': env.str('REDIS_RQ_URL'),
        'DEFAULT_TIMEOUT': 360,
    }
}

# EMAIL
# ------------------------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = env.str('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD')
DEFAULT_FROM_EMAIL = env.str('EMAIL_HOST_USER')

# REDIS
# ------------------------------------------------------------------------------
REDIS_HOST = 'localhost'
REDIS_PORT = '6379'
BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}

# CACHEOPS
# ------------------------------------------------------------------------------
CACHEOPS_REDIS = env.str('CACHEOPS_REDIS')
CACHEOPS = {
    'tools.proxy': {'ops': 'all', 'timeout': 60 * 60},
    'tools.provider': {'ops': 'all', 'timeout': 60 * 60},
    'iaccounts.account.AccountLinkHistory': {'ops': 'all', 'timeout': 60 * 60},
}

# S3
# ------------------------------------------------------------------------------
S3_ACCESS_KEY = env.str('S3_ACCESS_KEY')
S3_SECRET_KEY = env.str('S3_SECRET_KEY')

# Django storages
# ------------------------------------------------------------------------------
DEFAULT_FILE_STORAGE = 'gramotool.utils.storages.MediaRootS3Boto3Storage'
STATICFILES_STORAGE = 'gramotool.utils.storages.StaticRootS3Boto3Storage'

AWS_ACCESS_KEY_ID = S3_ACCESS_KEY
AWS_SECRET_ACCESS_KEY = S3_SECRET_KEY
AWS_STORAGE_BUCKET_NAME = 'gramotool'
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=604800',
}
AWS_DEFAULT_ACL = 'public-read'
AWS_QUERYSTRING_AUTH = False
AWS_S3_ENDPOINT_URL = 'https://fra1.digitaloceanspaces.com/'

MEDIA_URL = '/media/'
STATIC_URL = '/static/'
