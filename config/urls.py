from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps import views as sitemaps_views
from django.urls import include, path
from django.views import defaults as default_views
from django.views.decorators.cache import cache_page
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from config.sitemaps import InstagramAccountSitemap, StaticSitemap
from gramotool.views import get_metrics

schema_view = get_schema_view(
   openapi.Info(
      title="Gramotool API",
      default_version='v1',
   ),
   url='https://api2.gramotool.ru/',
   public=True,
)

sitemaps = {
    'static': StaticSitemap,
    'accounts': InstagramAccountSitemap,
}

# apps
urlpatterns = [
    path('tools/', include('gramotool.tools.urls')),
    path('compilations/', include('gramotool.compilations.urls')),
    path('articles/', include('gramotool.articles.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# other
urlpatterns += [
    path('sitemap.xml/', sitemaps_views.index, {'sitemaps': sitemaps, 'sitemap_url_name': 'sitemaps'}),
    path('sitemap-<section>.xml', cache_page(86400, cache='db_cache')(sitemaps_views.sitemap), {'sitemaps': sitemaps}, name='sitemaps'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('rq/', include('django_rq.urls')),
    path(settings.ADMIN_URL, admin.site.urls),
    path('metrics', get_metrics)
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
