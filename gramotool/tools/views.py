from tempfile import NamedTemporaryFile

from django.http import FileResponse
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView

from gramotool.http_response_helpers import (Error, ErrorSerializer,
                                             make_error_response,
                                             make_response)
from gramotool.iaccounts.models.accounts import AccountLinkHistory
from gramotool.iaccounts.serializers import AccountLinkHistorySerializer

from .serializers import (AccountInfoSerializer, AvatarSerializer,
                          FeedSerializer, HighlightsSerializer, IGTVSerializer,
                          SearchSerializer, StoriesSerializer)
from .services import (InstagramRequestError, InstagramTools, Tools,
                       WrongFileExtention)

max_id_param = openapi.Parameter('max_id', openapi.IN_QUERY, description="for pagination", type=openapi.TYPE_STRING)


class AccountStories(APIView):

    @swagger_auto_schema(operation_description="Get user stories",
                         responses={
                             status.HTTP_200_OK: StoriesSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)

        try:
            stories = InstagramTools().get_account_stories(username)
            return make_response(StoriesSerializer, stories)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountAvatar(APIView):

    @swagger_auto_schema(operation_description="Get user avatar",
                         responses={
                             status.HTTP_200_OK: AvatarSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)

        try:
            avatar = InstagramTools().get_account_avatar(username)
            return make_response(AvatarSerializer, avatar)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountFeed(APIView):

    @swagger_auto_schema(operation_description="Get specific user feed",
                         responses={
                             status.HTTP_200_OK: FeedSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         },
                         manual_parameters=[max_id_param])
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)
        max_id = self.request.GET.get('max_id', None)

        try:
            feed = InstagramTools().get_account_feed(username, max_id)
            return make_response(FeedSerializer, feed)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountTags(APIView):

    @swagger_auto_schema(operation_description="Get specific user tags feed",
                         responses={
                             status.HTTP_200_OK: FeedSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         },
                         manual_parameters=[max_id_param])
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)
        max_id = self.request.GET.get('max_id', None)

        try:
            tags_feed = InstagramTools().get_account_tags(username, max_id)
            return make_response(FeedSerializer, tags_feed)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountIGTV(APIView):

    @swagger_auto_schema(operation_description="Get user's igtv videos",
                         responses={
                             status.HTTP_200_OK: IGTVSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         },
                         manual_parameters=[max_id_param])
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)
        max_id = self.request.GET.get('max_id', None)

        try:
            igtv = InstagramTools().get_account_igtv(username, max_id)
            return make_response(IGTVSerializer, igtv)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountInfo(APIView):

    @swagger_auto_schema(operation_description="Get specific user account info",
                         responses={
                             status.HTTP_200_OK: AccountInfoSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)

        try:
            user_info = InstagramTools().get_account_info(username, ignore_private=True)
            return make_response(AccountInfoSerializer, user_info)
        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountExternalLinksL(ListAPIView):
    serializer_class = AccountLinkHistorySerializer

    @swagger_auto_schema(operation_description="Get user external links history",
                         responses={
                             status.HTTP_200_OK: AccountLinkHistorySerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        account_username = self.kwargs.get('username', None)
        queryset = AccountLinkHistory.objects.filter(account_history__iaccount__username=account_username)
        return queryset


class AccountHighlights(APIView):

    @swagger_auto_schema(operation_description="Get specific user highlights",
                         responses={
                             status.HTTP_200_OK: HighlightsSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        username = self.kwargs.get('username', None)

        try:
            highlights = InstagramTools().get_account_highlights(username)
            return make_response(HighlightsSerializer, highlights)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class AccountHighlightStories(APIView):

    @swagger_auto_schema(operation_description="Get highlight stories",
                         responses={
                             status.HTTP_200_OK: StoriesSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        highlight_id = self.kwargs.get('highlight_id', None)

        try:
            stories = InstagramTools().get_account_highlight_stories(highlight_id)
            return make_response(StoriesSerializer, stories)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class SearchAccounts(APIView):

    @swagger_auto_schema(operation_description="Users search",
                         responses={
                             status.HTTP_200_OK: SearchSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, **kwargs):
        search_query = self.kwargs.get('search_query', None).replace(' ', '')

        try:
            search_results = InstagramTools().search_accounts(search_query)
            return make_response(SearchSerializer, search_results)

        except InstagramRequestError as err:
            return make_error_response(Error(message=err.message))


class DownloadByUrl(APIView):

    @swagger_auto_schema(request_body=openapi.Schema(
                            type=openapi.TYPE_OBJECT,
                            properties={
                                'url': openapi.Schema(type=openapi.TYPE_STRING, description='string'),
                            }
                        ))
    def post(self, request, **kwargs):
        url = self.request.data.get('url')

        try:
            file_data, file_extention = Tools().get_file_data_and_ext_by_url(url)
            with NamedTemporaryFile() as tmp:
                tmp.write(file_data.content)
                response = FileResponse(open(tmp.name, 'rb'), filename=f'{tmp.name}.{file_extention}')
                response['Access-Control-Expose-Headers'] = 'Content-Disposition'
                return response
        except (InstagramRequestError, WrongFileExtention) as err:
            return make_error_response(Error(message=err.message))
