import datetime

from django_rq import job

from gramotool.iaccounts.services import InstagramAccountService

from .services_new.provider import ProviderService
from .services_new.proxy import ProxyService
from .services_new.working_account import WorkingAccountService
from .use_cases.manage_wa_work_readiness import ManageWAWorkReadinessUseCase


class RQJobs:
    def __init__(self,
                 proxy_service: ProxyService,
                 provider_service: ProviderService,
                 wa_service: WorkingAccountService,
                 manage_wa_work_readiness_use_case: ManageWAWorkReadinessUseCase,
                 ia_service: InstagramAccountService,
                 ) -> None:
        self.proxy_service = proxy_service
        self.provider_service = provider_service
        self.wa_service = wa_service
        self.manage_wa_work_readiness_use_case = manage_wa_work_readiness_use_case
        self.ia_service = ia_service

    def _make_prefix(self, func_name: str) -> str:
        return f'[SC][{func_name}][{datetime.datetime.utcnow()}]'

    def _log(self, prefix: str, message: str) -> None:
        print(f'{prefix}: {message}')

    @job('schedule')
    def check_disabled_proxies(self) -> None:
        prefix = '[SC][check_disabled_proxies]'
        print(prefix)

        proxies = self.proxy_service.get_not_active_proxies()

        if proxies.count() > 0:
            for proxy in proxies:
                proxy.check_proxy()

    @job('schedule')
    def check_providers(self) -> None:
        prefix = '[SC][check_providers]'
        print(prefix)

        self.provider_service.enable_providers_with_end_delay()
        self.provider_service.disable_used_providers()
        self.provider_service.init_providers()

    @job('schedule')
    def reauth_challenge_wa(self) -> None:
        prefix = '[SC][reauth_challenge_wa]'
        print(prefix)

        self.wa_service.reauth_accounts()

    @job('schedule')
    def manage_wa_work_readiness(self) -> None:
        prefix = self._make_prefix(self.manage_wa_work_readiness.__name__)
        self._log(prefix, 'starting')

        self.manage_wa_work_readiness_use_case.run()
        self._log(prefix, 'end')

    @job('schedule')
    def manage_accounts_media(self) -> None:
        prefix = self._make_prefix(self.manage_accounts_media.__name__)
        self._log(prefix, 'starting')

        self.ia_service.parse_media()
        self._log(prefix, 'end')
