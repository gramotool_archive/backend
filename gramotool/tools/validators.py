from re import compile

from django.core.validators import RegexValidator

proxy_pattern = compile(r'https?:\/\/[_0-9a-zA-Z]{1,64}:[_0-9a-zA-Z]{1,64}@([0-9]{0,3}.){4}:[0-9]{2,6}')

proxy_validator = RegexValidator(
    proxy_pattern, 'Не соответствует шаблону')
