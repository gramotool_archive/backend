from django.db.models.signals import post_save
from django.dispatch import receiver

from gramotool.services.instagram_api.client import ApiClient
from gramotool.services.instagram_api.errors import (AccountDisabledError,
                                                     ChallengeCodeSend,
                                                     ClientLoginError,
                                                     OnChallengeError)

from .models import WorkingAccount


@receiver(post_save, sender=WorkingAccount)
def activate_working_account(instance: WorkingAccount, created: bool, **kwargs) -> None:
    if instance.is_active and not instance.auth_settings and not instance.auth_error:
        try:
            auth_settings = ApiClient(
                username=instance.username,
                password=instance.password,
                proxy=instance.proxy
            ).login()
            instance.auth_settings = auth_settings
            instance.is_reauth_needed = False
            instance.save(update_fields=['auth_settings', 'is_reauth_needed'])

        except ClientLoginError:
            instance.auth_error = 'login_error'
            instance.is_active = False
            instance.save(update_fields=['auth_error', 'is_active'])

        except AccountDisabledError:
            instance.auth_error = 'account_disabled'
            instance.is_active = False
            instance.save(update_fields=['auth_error', 'is_active'])

        except ChallengeCodeSend as challenge:
            challenge_settings = getattr(challenge, 'challenge_settings', None)
            challenge_url = getattr(challenge, 'challenge_url', None)

            instance.auth_error = challenge
            instance.is_active = False
            instance.challenge_url = challenge_url
            instance.challenge_settings = challenge_settings
            instance.save(update_fields=['auth_error', 'challenge_url', 'challenge_settings', 'is_active'])

    if instance.auth_error == 'enter_code' and instance.challenge_code:
        try:
            ApiClient(
                username=instance.username,
                password=instance.password,
                proxy=instance.proxy,
                auth_settings=instance.challenge_settings
            ).challenge_send_code(instance.challenge_url, instance.challenge_code)
        except OnChallengeError as error:
            instance.auth_error = error
            instance.is_active = False
            instance.save(update_fields=['auth_error', 'is_active'])


@receiver(post_save, sender=WorkingAccount)
def post_save_working_account(instance: WorkingAccount, created: bool, **kwargs) -> None:
    if instance.provider:
        instance.provider.check_provider()
