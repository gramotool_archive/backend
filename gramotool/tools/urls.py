from django.urls import path

from .views import (AccountAvatar, AccountExternalLinksL, AccountFeed,
                    AccountHighlights, AccountHighlightStories, AccountIGTV,
                    AccountInfo, AccountStories, AccountTags, DownloadByUrl,
                    SearchAccounts)

urlpatterns = [
    path('account/<str:username>/stories/', AccountStories.as_view(), name='get-account-stories'),
    path('account/<str:username>/avatar/', AccountAvatar.as_view(), name='get-account-avatar'),
    path('account/<str:username>/highlights/', AccountHighlights.as_view(), name='get-account-highlights'),
    path('account/<str:username>/feed/', AccountFeed.as_view(), name='get-account-feed'),
    path('account/<str:username>/tags/', AccountTags.as_view(), name='get-account-tags'),
    path('account/<str:username>/info/', AccountInfo.as_view(), name='get-account-info'),
    path('account/<str:username>/igtv/', AccountIGTV.as_view(), name='get-account-igtv'),
    path('account/<str:username>/external-links/', AccountExternalLinksL.as_view(), name='get-account-external-links'),

    path('search/<str:search_query>/', SearchAccounts.as_view(), name='search-accounts'),
    path('highlight/<int:highlight_id>/', AccountHighlightStories.as_view(), name='get-account-highlight-stories'),
    path('download/', DownloadByUrl.as_view(), name='get-file-by-url'),
]
