import datetime

from django.db import transaction

from gramotool.tools.services_new.working_account import WorkingAccountService

WORKING_ACCOUNT_ASSIGNED_TO_WORK_COUNT = 6


class NotEnoughWorkingAccountsError(Exception):
    pass


class ManageWAWorkReadinessUseCase:
    """
    Lay of current active accounts and assign to work fresh ones
    """

    def __init__(self,
                 wa_service: WorkingAccountService,
                 ) -> None:
        self.wa_service = wa_service

    def _make_prefix(self) -> str:
        return f'[{self.__class__.__name__}][{datetime.datetime.utcnow()}]'

    def _log(self, prefix: str, message: str) -> None:
        print(f'{prefix}: {message}')

    def run(self) -> None:
        prefix = self._make_prefix()
        get_ready_for_a_work_assigment_accounts_ids = list(
            self.wa_service.get_ready_for_a_work_assigment().values_list('id', flat=True)
        )

        self._log(prefix, f'get_ready_for_a_work_assigment_accounts_ids {get_ready_for_a_work_assigment_accounts_ids}')

        current_active_accounts_ids = [a.id for a in self.wa_service.get_ready_to_work()]
        self._log(prefix, f'current_active_accounts_ids {current_active_accounts_ids}')

        if len(get_ready_for_a_work_assigment_accounts_ids) < WORKING_ACCOUNT_ASSIGNED_TO_WORK_COUNT:
            self.wa_service.lay_off(pks=current_active_accounts_ids)
            # unfortunately rq doe not raise
            # TODO: send to sentry manually
            raise NotEnoughWorkingAccountsError

        account_ids_to_assign = get_ready_for_a_work_assigment_accounts_ids[:WORKING_ACCOUNT_ASSIGNED_TO_WORK_COUNT]
        self._log(prefix, f'account_ids_to_assign {account_ids_to_assign}')

        with transaction.atomic():
            self.wa_service.assign_to_work(pks=account_ids_to_assign)
            self.wa_service.lay_off(pks=current_active_accounts_ids)
