from gramotool.tools.services_new.working_account import (
    NoAvailableActiveWorkingAccountError, WorkingAccountService)

from ..models import WorkingAccount


class CantGetMostChillyWorkingAccount(Exception):
    pass


class GetMostChillyWorkingAccountUseCase:

    def __init__(self,
                 wa_service: WorkingAccountService,
                 ) -> None:
        self.wa_service = wa_service

    def run(self) -> WorkingAccount:
        """Returns the most chill working account"""
        try:
            wa = self.wa_service.get_random_chilly_working_account()
        except NoAvailableActiveWorkingAccountError:
            raise CantGetMostChillyWorkingAccount

        return wa
