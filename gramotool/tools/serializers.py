from rest_framework.serializers import (BooleanField, CharField, IntegerField,
                                        Serializer, URLField)
from rest_framework_dataclasses.serializers import DataclassSerializer

from gramotool.services.instagram_api.serializers.feed import InstagramUserFeed
from gramotool.services.instagram_api.serializers.igtv import InstagramIGTV
from gramotool.services.instagram_api.serializers.stories import \
    InstagramUserStory
from gramotool.services.instagram_api.serializers.user import InstagramUserInfo


class _AccountInfoSerializer(DataclassSerializer):
    class Meta:
        dataclass = InstagramUserInfo


class AccountInfoSerializer(Serializer):
    data = _AccountInfoSerializer()


class RBMediaInfo(Serializer):
    url = URLField(required=True)


class _StorySerializer(DataclassSerializer):
    class Meta:
        dataclass = InstagramUserStory


class StoriesSerializer(Serializer):
    data = _StorySerializer(many=True)


class AvatarSerializer(Serializer):
    data = CharField()


class _FeedSerializer(DataclassSerializer):
    class Meta:
        dataclass = InstagramUserFeed


class FeedSerializer(Serializer):
    data = _FeedSerializer()


class HighlightSerializer(Serializer):
    id = CharField()
    title = CharField()
    media_count = IntegerField()
    cover = CharField()


class HighlightsSerializer(Serializer):
    data = HighlightSerializer(many=True)


class _SmallUserSerializer(Serializer):
    pk = IntegerField()
    username = CharField()
    full_name = CharField()
    profile_pic_url = CharField()
    is_private = BooleanField()
    is_verified = BooleanField()


class SearchSerializer(Serializer):
    data = _SmallUserSerializer(many=True)


class _IGTVSerializer(DataclassSerializer):
    class Meta:
        dataclass = InstagramIGTV


class IGTVSerializer(Serializer):
    data = _IGTVSerializer()
