from datetime import datetime

from django.contrib.postgres.fields import JSONField
from django.db import models

from gramotool.tools.utils import proxy_checker

from .validators import proxy_validator


class Proxy(models.Model):
    PROXY_PROVIDERS = (
        ('proxy.market', 'proxy.market'),
        ('shopproxy.net', 'shopproxy.net'),
        ('proxy6.net', 'proxy6.net'),
        ('proxy-store.com', 'proxy-store.com'),
    )

    service_provider = models.CharField(choices=PROXY_PROVIDERS, max_length=20, null=True, blank=True)
    value = models.CharField(max_length=128, unique=True, validators=[proxy_validator])

    last_check = models.DateTimeField(auto_now_add=True)
    down_time = models.DateTimeField(blank=True, null=True)
    disable_reason = models.CharField(max_length=50, null=True, blank=True)

    is_active = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f'[{self.id}] [{self.address}] [{self.service_provider}]'

    class Meta:
        verbose_name = 'Прокси'
        verbose_name_plural = 'Прокси'

    def mark_as_up(self) -> None:
        self.is_active = True
        self.down_time = None
        self.last_check = datetime.now()
        self.save(update_fields=['is_active', 'down_time', 'last_check'])

    def check_proxy(self) -> None:
        current_time = datetime.now()
        diff = current_time - self.last_check

        if diff.seconds >= 40:
            self.is_active = proxy_checker(self)
            self.last_check = datetime.now()
            self.save(update_fields=['last_check', 'is_active'])

    @property
    def address(self) -> str:
        address = self.value.split('@')[1]
        return address

    @property
    def user(self) -> str:
        auth = self.value.split('@')[0].split('//')[1]
        return auth.split(':')[0]

    @property
    def password(self) -> str:
        auth = self.value.split('@')[0].split('//')[1]
        return auth.split(':')[1]


class WorkingAccount(models.Model):
    username = models.CharField(max_length=50, unique=True, verbose_name='Имя пользователя')
    password = models.CharField(max_length=50, verbose_name='Пароль')

    provider = models.ForeignKey('tools.Provider', on_delete=models.SET_NULL, related_name='working_accounts',
                                 null=True, blank=True, verbose_name='Провайдер')

    auth_timestamp = models.DateTimeField(blank=True, null=True)
    auth_settings = JSONField(blank=True, null=True)
    auth_error = models.CharField(max_length=128, blank=True, null=True)

    used_last_time = models.DateTimeField(auto_now_add=True)

    challenge_url = models.CharField(max_length=120, blank=True, null=True)
    challenge_code = models.CharField(max_length=10, blank=True, null=True)
    challenge_settings = JSONField(blank=True, null=True)

    disable_reason = models.CharField(max_length=100, null=True, blank=True)

    is_active = models.BooleanField(default=False, verbose_name='Активный')
    is_replay_challange = models.BooleanField(default=False)
    is_reauth_needed = models.BooleanField(default=False)
    is_ready_to_work = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Рабочий аккаунт'
        verbose_name_plural = 'Рабочие аккаунты'

    def __str__(self) -> str:
        return f'{self.username}[{self.id}]'

    def reauth(self) -> None:
        self.is_active = True
        self.disable_reason = None
        self.auth_settings = None
        self.save(update_fields=['is_active', 'disable_reason', 'auth_settings'])

    @property
    def proxy(self) -> Proxy:
        return self.provider.proxy


class Provider(models.Model):
    proxy = models.OneToOneField('Proxy', related_name='provider', on_delete=models.PROTECT)

    error = models.CharField(max_length=50, null=True, blank=True)

    start_time = models.DateTimeField(null=True, blank=True)
    delay_end_time = models.DateTimeField(null=True, blank=True)
    used_last_time = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=False)
    is_on_delay = models.BooleanField(default=False)

    @property
    def is_proxy_active(self) -> bool:
        return True if self.proxy.is_active else False

    @property
    def is_working_accounts_active(self) -> bool:
        return True if self.working_accounts.filter(is_active=True).exists() else False

    def check_provider(self) -> None:
        is_working = True if self.is_proxy_active and self.is_working_accounts_active else False

        if not is_working:
            self.error = 'proxy or accounts inactive'
            self.start_time = None
            self.is_active = False
        else:
            self.error = None

        self.save(update_fields=['start_time', 'error', 'is_active'])

    def start_provider(self) -> None:
        self.used_last_time = datetime.now()

        if self.error is None:
            self.start_time = datetime.now()
            self.is_active = True
            self.is_on_delay = False
            self.delay_end_time = None
            self.save(update_fields=['used_last_time', 'start_time', 'is_active', 'delay_end_time', 'is_on_delay'])

        self.save(update_fields=['used_last_time'])

    class Meta:
        verbose_name = 'Провайдер'
        verbose_name_plural = 'Провайдеры'

    def __str__(self) -> str:
        return f'[{self.id}] PROVIDER'
