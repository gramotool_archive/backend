import typing as t

import requests
from django.core.cache import cache

from gramotool.iaccounts.tasks import (parse_account_external_url,
                                       update_account_info)
from gramotool.resource_provider import rp
from gramotool.services.instagram_api.client import (
    ApiClient, InstagramApiBadResponseDataError)
from gramotool.services.instagram_api.errors import (AccountDelayError,
                                                     AccountIsPrivateError,
                                                     AccountNotFoundError,
                                                     ClientChallengeError,
                                                     LoginRequiredError)
from gramotool.services.instagram_api.serializers.feed import InstagramUserFeed
from gramotool.services.instagram_api.serializers.highlights import \
    InstagramUserHighlight
from gramotool.services.instagram_api.serializers.igtv import InstagramIGTV
from gramotool.services.instagram_api.serializers.stories import \
    InstagramUserStory
from gramotool.services.instagram_api.serializers.user import (
    InstagramUserInfo, InstagramUserInfoSmall)

from . import errors


class InstagramRequestError(Exception):
    def __init__(self, message: str) -> None:
        self.message = message


class WrongFileExtention(Exception):
    def __init__(self, message: str) -> None:
        self.message = message


class AccountsProvider:

    def get_api(self) -> ApiClient:
        working_account = rp.get_most_chilly_working_account_use_case.run()

        print(f'AccountsProvider [{working_account.username}]')
        api = ApiClient(
            username=working_account.username,
            password=working_account.password,
            auth_settings=working_account.auth_settings,
            proxy=working_account.proxy
        )
        return api


class Tools:

    def get_file_data_and_ext_by_url(self, url: str):
        filename = url.split('?')[0]
        file_extention = filename.split('.')[-1]

        if file_extention in ['mp4', 'jpg']:
            file_data = requests.get(url, timeout=3)
            return file_data, file_extention
        else:
            raise WrongFileExtention(message='Расширение файла не поддерживается')


class InstagramTools:

    def get_account_info(self, username: str, ignore_private=False) -> InstagramUserInfo:
        cache_key = f'{username}-info'
        account_info = cache.get(cache_key)

        if account_info is None:
            api = AccountsProvider().get_api()

            try:
                account_info = api.get_account_info(username)
                cache.set(cache_key, account_info, 1800)
                update_account_info.delay(account_info)
                parse_account_external_url.delay(account_info)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except (InstagramApiBadResponseDataError, AccountDelayError):
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountDelayError as err:
                raise AccountDelayError(err.api_username)

        if account_info.is_private and not ignore_private:
            raise InstagramRequestError(message='Приватный аккаунт')

        return account_info

    def get_account_avatar(self, username: str) -> str:
        """ Get account avatar """

        cache_key = f'{username}-avatar'
        account_avatar = cache.get(cache_key)

        if account_avatar is None:
            try:
                account_info = self.get_account_info(username, ignore_private=True)
                account_avatar = account_info.profile_pic_url
                cache.set(cache_key, account_avatar, 600)

            except (ClientChallengeError, LoginRequiredError, AccountDelayError):
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

        return account_avatar

    def get_account_stories(self, username: str) -> t.List[InstagramUserStory]:
        """ Get account stories list """

        cache_key = f'{username}-stories'
        account_stories = cache.get(cache_key)

        if account_stories is None:
            api = AccountsProvider().get_api()

            try:
                account_info = self.get_account_info(username)
                account_stories = api.get_account_stories(account_info.pk)
                cache.set(cache_key, account_stories, 300)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть истории приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_stories

    def get_account_feed(self, username: str, max_id: t.Optional[str]) -> InstagramUserFeed:
        """ Get the feed for the specified username """

        cache_key = f'{username}-{max_id}-feed' if max_id else f'{username}-feed'
        account_feed = cache.get(cache_key)

        if account_feed is None:
            api = AccountsProvider().get_api()

            try:
                account_info = self.get_account_info(username)
                account_feed = api.get_account_feed(account_info.pk, max_id)
                cache.set(cache_key, account_feed, 3000)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть ленту приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_feed

    def get_account_tags(self, username: str, max_id: t.Optional[str]) -> InstagramUserFeed:
        """ Get the tags for the specified username """

        cache_key = f'{username}-{max_id}-tags' if max_id else f'{username}-tags'
        account_tags = cache.get(cache_key)

        if account_tags is None:
            api = AccountsProvider().get_api()

            try:
                account_info = self.get_account_info(username)
                account_tags = api.get_account_tags(account_info.pk, max_id)
                cache.set(cache_key, account_tags, 3000)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть отметки приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_tags

    def get_account_igtv(self, username: str, max_id: t.Optional[str]) -> InstagramIGTV:
        """ Get the user's igtv """

        cache_key = f'{username}-{max_id}-igtv' if max_id else f'{username}-igtv'
        account_igtv = cache.get(cache_key)

        if account_igtv is None:
            api = AccountsProvider().get_api()

            try:
                account_info = self.get_account_info(username)
                account_igtv = api.get_account_igtv(account_info.pk, max_id)
                cache.set(cache_key, account_igtv, 3000)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть igtv приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_igtv

    def get_account_highlights(self, username: str) -> t.List[InstagramUserHighlight]:
        """ Get account highlights """

        cache_key = f'{username}-highlights'
        account_highlights = cache.get(cache_key)

        if account_highlights is None:
            api = AccountsProvider().get_api()

            try:
                account_info = self.get_account_info(username)
                account_highlights = api.get_account_highlights(account_info.pk)
                cache.set(cache_key, account_highlights, 800)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть хайлайты приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_highlights

    def get_account_highlight_stories(self, highlight_id: str) -> t.List[InstagramUserStory]:
        """ Get account highlight stories """

        cache_key = f'{highlight_id}-highlight-stories'
        account_highlight_stories = cache.get(cache_key)

        if account_highlight_stories is None:
            api = AccountsProvider().get_api()
            highlight_id = f"highlight:{highlight_id}"

            try:
                account_highlight_stories = api.get_account_highlight_stories(highlight_id)
                cache.set(cache_key, account_highlight_stories, 800)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except AccountNotFoundError:
                raise InstagramRequestError(message=errors.user_not_found)

            except AccountIsPrivateError:
                raise InstagramRequestError(message='Нельзя смотреть хайлайты приватного аккаунта')

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return account_highlight_stories

    def search_accounts(self, search_query: str) -> t.List[InstagramUserInfoSmall]:
        """ Search account """

        cache_key = f'{search_query}-search'
        search_result = cache.get(cache_key)

        if search_result is None:
            api = AccountsProvider().get_api()

            try:
                search_result = api.search_accounts(search_query).users
                cache.set(cache_key, search_result, 6000)

            except (ClientChallengeError, LoginRequiredError):
                rp.working_account_service.mark_as_reauth(username=api.username)
                raise InstagramRequestError(message=errors.instagram_request_error)

            except (ConnectionError, TimeoutError):
                api.proxy.check_proxy()
                raise InstagramRequestError(message=errors.instagram_request_error)

        return search_result
