import random
import typing as t

from django.db.models.functions import Now

from gramotool.tools.models import WorkingAccount
from gramotool.type_helpers import Query


class NoAvailableActiveWorkingAccountError(Exception):
    pass


class WorkingAccountService:
    model = WorkingAccount

    def get_random_chilly_working_account(self) -> WorkingAccount:
        accounts = self.model.objects.filter(is_active=True, provider__is_active=True)

        if not accounts:
            raise NoAvailableActiveWorkingAccountError

        account_ids = list(accounts.values_list('id', flat=True))
        random_account_id = random.choice(account_ids)

        return accounts.get(id=random_account_id)

    def get_active(self) -> Query[WorkingAccount]:
        return self.model.objects.filter(is_active=True)

    def get_ready_to_work(self) -> Query[WorkingAccount]:
        return self.model.objects.filter(is_ready_to_work=True)

    def get_ready_for_a_work_assigment(self) -> Query[WorkingAccount]:
        return self.model.objects \
            .filter(is_active=True, is_ready_to_work=False, provider__is_active=True) \
            .prefetch_related('provider')

    def assign_to_work(self, pks: t.Sequence[int]) -> None:
        self.model.objects \
            .filter(id__in=pks) \
            .update(is_ready_to_work=True)

    def lay_off(self, pks: t.Sequence[int]) -> None:
        self.model.objects \
            .filter(id__in=pks) \
            .update(is_ready_to_work=False, used_last_time=Now())

    def disable(self, username: str, disable_reason: str) -> None:
        self.model.objects \
            .filter(username=username) \
            .update(is_active=False, disable_reason=disable_reason, auth_settings=None)

    def mark_as_reauth(self, username: str) -> None:
        self.model.objects \
            .filter(username=username) \
            .update(is_active=False, is_reauth_needed=True)

    @staticmethod
    def reauth_accounts() -> None:
        working_accounts = WorkingAccount.objects.filter(is_reauth_needed=True, auth_error__isnull=True)

        for wa in working_accounts:
            wa.reauth()
