import typing as t

from gramotool.tools.models import Proxy


class ProxyService:

    @staticmethod
    def get_not_active_proxies() -> t.List[Proxy]:
        return Proxy.objects.only('last_check', 'is_active', 'value').filter(is_active=False)
