from datetime import datetime, timedelta

from django.conf import settings

from gramotool.tools.models import Provider


class NoAvailableActiveProviderError(Exception):
    pass


class NoAvailableActiveWorkingAccountError(Exception):
    pass


class ProviderService:

    @staticmethod
    def init_providers() -> None:
        active_providers_count = Provider.objects.filter(error__isnull=True, is_active=True).count()
        ready_providers_count = Provider.objects.filter(error__isnull=True, is_active=False, is_on_delay=False).count()

        if active_providers_count < settings.MIN_ACTIVE_PROVIDERS_COUNT:

            while active_providers_count < settings.MIN_ACTIVE_PROVIDERS_COUNT and ready_providers_count != 0:
                Provider.objects \
                    .filter(is_active=False, error__isnull=True, is_on_delay=False) \
                    .earliest('used_last_time').start_provider()

                ready_providers_count = Provider.objects.filter(error__isnull=True, is_active=False,
                                                                is_on_delay=False).count()
                active_providers_count = Provider.objects.filter(error__isnull=True, is_active=True).count()

    @staticmethod
    def disable_used_providers() -> None:
        provider_working_time = settings.PROVIDER_WORKING_TIME * 60 * 60
        min_start_time = datetime.now() - timedelta(seconds=provider_working_time)
        delay_end_time = datetime.now() + timedelta(seconds=provider_working_time) - timedelta(seconds=300)

        Provider.objects \
            .filter(is_active=True, start_time__lte=min_start_time) \
            .update(is_active=False, start_time=None, is_on_delay=True, delay_end_time=delay_end_time)

    @staticmethod
    def enable_providers_with_end_delay() -> None:
        Provider.objects \
            .filter(is_on_delay=True, error__isnull=True, delay_end_time__lte=datetime.now()) \
            .update(is_active=False, is_on_delay=False, delay_end_time=None)
