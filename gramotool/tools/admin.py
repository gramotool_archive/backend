import typing as t

from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Provider, Proxy, WorkingAccount


class WorkingAccountInline(admin.StackedInline):
    model = WorkingAccount
    extra = 0
    fields = ['is_active', 'username', 'password', 'auth_settings']


@admin.register(WorkingAccount)
class WorkingAccountAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'is_active', 'is_reauth_needed', 'is_ready_to_work',
                    'used_last_time', 'disable_reason', 'provider']
    readonly_fields = ['used_last_time']
    fieldsets = (
        ('Данные', {
            'fields': (
                ('is_active', 'is_reauth_needed', 'is_ready_to_work'),
                ('provider',),
                ('disable_reason',),
            )
        }),
        ('Auth', {
            'fields': (
                ('username', 'password',),
                ('auth_settings',),
                ('auth_error',),
                ('challenge_url', 'challenge_code', 'is_replay_challange'),
                ('challenge_settings',),
            )
        }),
        ('Meta', {
            'fields': (
                ('used_last_time',),
            )
        }),
    )

    def provider(self, instance: WorkingAccount) -> t.Optional[str]:
        if not instance.provider:
            return None

        color = 'green' if instance.provider.is_active else 'red'
        result = f'<span style="color:{color};">PROVIDER[{instance.provider.id}]</span>'
        return mark_safe(result)

    provider.allow_tags = True


@admin.register(Proxy)
class ProxyAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'is_active', 'down_time', 'last_check', 'provider']
    list_filter = ['is_active']
    readonly_fields = ['down_time']


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'is_active', 'is_on_delay', 'error', 'get_accounts', 'used_last_time', 'start_time',
                    'delay_end_time']
    inlines = [WorkingAccountInline]

    def get_accounts(self, instance: Provider) -> str:
        result = ''
        working_accounts = instance.working_accounts.all()

        for working_account in working_accounts:
            color = 'green' if working_account.is_active else 'red'
            working_account_str = f'<span style="color:{color};">' \
                                  f'{working_account.username}[{working_account.id}]</span> '
            result += working_account_str

        return mark_safe(result)

    get_accounts.allow_tags = True
