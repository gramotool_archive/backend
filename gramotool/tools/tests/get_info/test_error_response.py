import typing as t
from collections import OrderedDict
from unittest.mock import Mock, patch

import pytest
from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from gramotool.resource_provider import rp
from gramotool.services.instagram_api.client import ApiClient

URL = reverse('get-account-info', kwargs={'username': 'test-user'})


@pytest.fixture
def make_error_response() -> t.Callable[[], t.Dict]:
    def _wrap() -> t.Dict:
        return {
            'error': OrderedDict(
                [('message', 'Ошибка при обращении к Instagram. Попробуйте еще раз'),
                 ('type', None),
                 ('code', None)]
            )
        }

    return _wrap


def test_no_root_key(client, make_working_account, get_call_api_mock, make_error_response):
    rp.get_most_chilly_working_account_use_case.run = Mock(return_value=make_working_account())

    insta_response_json = {'bad_payload': 'here'}

    with patch.object(ApiClient, 'call_api', return_value=get_call_api_mock(insta_response_json)):
        cache.clear()
        response = client.get(URL, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST

        assert response.data == make_error_response()


def test_bad_user_body(client, make_working_account, make_insta_response, get_call_api_mock, make_error_response):
    rp.get_most_chilly_working_account_use_case.run = Mock(return_value=make_working_account())

    insta_response = make_insta_response()

    del insta_response['user']['username']

    with patch.object(ApiClient, 'call_api', return_value=get_call_api_mock(insta_response)):
        cache.clear()
        response = client.get(URL, format='json')

        assert response.status_code == status.HTTP_400_BAD_REQUEST

        assert response.data == make_error_response()
