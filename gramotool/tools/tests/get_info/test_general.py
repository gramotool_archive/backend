from unittest.mock import Mock, patch

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status

from gramotool.resource_provider import rp
from gramotool.services.instagram_api.client import ApiClient


def test_from_cash(client,
                   make_working_account,
                   make_insta_response,
                   get_call_api_mock,
                   build_expected_response):
    url = reverse('get-account-info', kwargs={'username': 'test-user'})

    rp.get_most_chilly_working_account_use_case.run = Mock(return_value=make_working_account())

    with patch.object(ApiClient, 'call_api', return_value=get_call_api_mock(make_insta_response())) as mocked_call_api:
        # make 2 requests to get from api and then from cache
        cache.clear()
        for _ in range(2):
            response = client.get(url, format='json')

            assert response.data.get('data')

            assert response.status_code == status.HTTP_200_OK
            assert dict(response.data['data']) == build_expected_response()

        assert mocked_call_api.call_count == 1


def test_hd_profile_pic_url(client,
                            make_working_account,
                            make_insta_response,
                            get_call_api_mock,
                            build_expected_response):
    url = reverse('get-account-info', kwargs={'username': 'test-user'})

    rp.get_most_chilly_working_account_use_case.run = Mock(return_value=make_working_account())

    insta_response = make_insta_response()
    insta_response['user']['hd_profile_pic_url_info'] = {'url': 'test-hd-pofile-pic-url'}

    expected_response = build_expected_response()
    expected_response['profile_pic_url'] = 'test-hd-pofile-pic-url'

    with patch.object(ApiClient, 'call_api', return_value=get_call_api_mock(insta_response)) as mocked_call_api:
        cache.clear()
        response = client.get(url, format='json')

        assert response.data.get('data')

        assert response.status_code == status.HTTP_200_OK

        assert dict(response.data['data']) == expected_response

        assert mocked_call_api.call_count == 1


def test_ignore_private_account_error(client,
                                      make_working_account,
                                      make_insta_response,
                                      get_call_api_mock,
                                      build_expected_response):
    url = reverse('get-account-info', kwargs={'username': 'test-user'})

    rp.get_most_chilly_working_account_use_case.run = Mock(return_value=make_working_account())

    insta_response = make_insta_response()
    insta_response['user']['is_private'] = True

    expected_response = build_expected_response()
    expected_response['is_private'] = True

    with patch.object(ApiClient, 'call_api', return_value=get_call_api_mock(insta_response)) as mocked_call_api:
        cache.clear()
        response = client.get(url, format='json')

        assert response.data.get('data')

        assert response.status_code == status.HTTP_200_OK

        assert dict(response.data['data']) == expected_response

        assert mocked_call_api.call_count == 1
