import typing as t
from unittest.mock import Mock

import pytest
from requests import Response

from ..models import Proxy, WorkingAccount


@pytest.fixture
def make_insta_response() -> t.Callable[[], t.Dict]:
    def _wrap() -> t.Dict:
        return {
            'user': {
                'pk': 123,
                'username': 'test-insta-username',
                'full_name': 'test-insta-fullname',
                'biography': 'test-insta-fullname',
                'external_url': 'test-insta-fullname',
                'follower_count': 123,
                'following_count': 123,
                'media_count': 123,
                'total_igtv_videos': 123,
                'usertags_count': 123,
                'is_private': False,
                'is_verified': False,
                'is_business': False,
                'profile_pic_url': 'url',
                'hd_profile_pic_versions': [
                    {'url': 'test-url'}
                ]
            }
        }

    return _wrap


@pytest.fixture
def build_expected_response() -> t.Callable[[], t.Dict]:
    def _wrap() -> t.Dict:
        return {
            'pk': 123,
            'username': 'test-insta-username',
            'full_name': 'test-insta-fullname',
            'biography': 'test-insta-fullname',
            'external_url': 'test-insta-fullname',
            'follower_count': 123,
            'following_count': 123,
            'media_count': 123,
            'total_igtv_videos': 123,
            'usertags_count': 123,
            'profile_pic_s_url': 'url',
            'profile_pic_url': 'test-url',
            'is_private': False,
            'is_verified': False,
            'is_business': False,
            'public_email': None,
            'contact_phone_number': None,
        }

    return _wrap


@pytest.fixture
def auth_settings() -> t.Dict:
    ua = "Instagram 121.0.0.29.119 Android (24/7.0; 640dpi; 1440x2560; HUAWEI; LON-L29; HWLON; hi3660; en_US)"
    return {
        "uuid": "455153fc-6f72-4194-be28-32932e361bd1",
        "ad_id": "cb170029-829d-4b4e-8e09-045d264599be",
        "cookies": {
            "mid": "XzuX3AABAAHRD6bauCqu-NRqer5B", "rur": "FRC",
            "urlgen": "\"{2a0f:d003:d067:7ffd:e618:6bff:fe19:9536: 208475}:1k7xQU:SPA8jzIY3HT-4Lqt2oz184Ohntg\"",
            "ds_user": "dizain9111", "csrftoken": "fk9cZa6YNN7l4FKaZwoI9yGKDkLJYCyQ",
            "sessionid": "34747458055%3AVPEUvEOdQZY8FG%3A16", "ds_user_id": "34747458055"},
        "headers": {
            "Accept": "*/*", "Connection": "keep-alive",
            "User-Agent": ua,
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Accept-Encoding": "gzip, deflate"},
        "phone_id": "28ea47ed-9552-42c1-90d8-ad1ea617e590",
        "device_id": "android-63eae29de4cc0024",
        "signature": "ig_sig_key_version=4&signed_body=body"
    }


@pytest.fixture
def make_working_account(auth_settings) -> t.Callable[[], WorkingAccount]:
    def _wrap() -> WorkingAccount:
        proxy = Mock(spec_set=Proxy)
        proxy.value = 'https://test-proxy-user:test-proxy-passord@123.123.123.123:1111'

        working_account = Mock(spec_set=WorkingAccount)
        working_account.username = 'wa-test-username'
        working_account.password = 'wa-test-password'
        working_account.auth_settings = auth_settings
        working_account.proxy = proxy

        return working_account

    return _wrap


@pytest.fixture
def get_call_api_mock() -> t.Callable[[], Response]:
    def _wrap(data: t.Dict) -> Response:
        api_resp = Mock(spec_set=Response)
        api_resp.json = Mock(return_value=data)

        return api_resp

    return _wrap
