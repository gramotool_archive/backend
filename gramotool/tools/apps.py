from django.apps import AppConfig


class ToolsConfig(AppConfig):
    name = 'gramotool.tools'
    verbose_name = 'Tools'

    def ready(self) -> None:
        from gramotool.resource_provider import rp  # noqa F401
        from gramotool.tools import signals  # noqa F401
