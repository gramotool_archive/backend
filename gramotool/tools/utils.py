import typing as t

import requests

if t.TYPE_CHECKING:
    from .models import Proxy


class InstagramID:
    """
    Utility class to convert between IG's internal numeric ID and the shortcode used in weblinks.
    Does NOT apply to private accounts.
    """
    ENCODING_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'

    @staticmethod
    def _encode(num: int, alphabet=ENCODING_CHARS) -> str:
        """Covert a numeric value to a shortcode."""
        if num == 0:
            return alphabet[0]

        arr = []
        base = len(alphabet)

        while num:
            rem = num % base
            num //= base
            arr.append(alphabet[rem])

        arr.reverse()

        return ''.join(arr)

    @staticmethod
    def _decode(shortcode: str, alphabet=ENCODING_CHARS) -> int:
        """Covert a shortcode to a numeric value."""
        base = len(alphabet)
        strlen = len(shortcode)
        num = 0
        idx = 0

        for char in shortcode:
            power = (strlen - (idx + 1))
            num += alphabet.index(char) * (base ** power)
            idx += 1

        return num


class MediaInfoParser:

    def __init__(self, media_info: t.Dict) -> None:
        self.media_info = media_info

    def _parse(self) -> t.List[t.Dict]:
        response = []

        media = self.media_info['items'][0]

        if media['media_type'] == 8:  # if carousel
            for item_info in media['carousel_media']:
                response.append(self._get_item(item_info))
        else:
            response.append(self._get_item(media))

        return response

    def _get_item(self, item_info) -> t.Dict:
        item = {
            'media_type': item_info['media_type'],
        }

        if item_info['media_type'] == 2:  # if video
            item['media_cover'] = item_info['image_versions2']['candidates'][0]['url']
            item['media_src'] = item_info['video_versions'][0]['url']

        if item_info['media_type'] == 1:  # if photo
            item['media_src'] = item_info['image_versions2']['candidates'][0]['url']

        return item


def proxy_checker(proxy: 'Proxy') -> bool:
    URL = "https://www.google.ru/"

    s = requests.Session()
    s.proxies = {'https': proxy.value}

    try:
        r = s.get(URL, timeout=3)

        if r.status_code != 200:
            return False
        return True
    except Exception:
        return False
