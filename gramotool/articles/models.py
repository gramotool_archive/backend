import random
import uuid

from django.core.validators import validate_image_file_extension
from django.db import models
from imagekit.models import ImageSpecField
from pytils.translit import slugify

from gramotool.iaccounts.models.base import Base


def get_upload_path(instance, filename):
    extension = filename.split('.')[-1]
    return f'{instance.get_upload_path()}/{uuid.uuid4()}.{extension}'


class Article(Base):
    title = models.CharField('Заголовок', max_length=100)
    slug = models.SlugField(max_length=100, blank=True)
    content = models.TextField()

    cover = models.ImageField('Обложка', null=True, upload_to=get_upload_path,
                              validators=[validate_image_file_extension])
    cover_spec = ImageSpecField(source='cover', format='JPEG', options={'quality': 85})

    is_active = models.BooleanField('Показывать на сайте', default=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = slugify(self.title)
            if Article.objects.filter(slug=slug).exists():
                slug = self.generate_unique_slug(base_slug=slug)
            self.slug = slug
        super().save(*args, **kwargs)

    @staticmethod
    def generate_unique_slug(base_slug: str) -> str:
        slug = f'{base_slug}-{str(random.randint(0, 99))}'
        while Article.objects.filter(slug=slug).exists():
            slug = f'{base_slug}-{str(random.randint(0, 99))}'
        return slug

    def get_upload_path(self):
        return f'articles/{self.id}/cover'

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class ArticleImage(models.Model):
    article = models.ForeignKey(Article, related_name='images', on_delete=models.CASCADE)
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    image = models.ImageField('Изображение', upload_to=get_upload_path, validators=[validate_image_file_extension])
    img = ImageSpecField(source='image', format='JPEG', options={'quality': 85})

    def get_upload_path(self):
        return f'articles/{self.article.id}'
