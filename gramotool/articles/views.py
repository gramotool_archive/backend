from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status

from gramotool.http_response_helpers import ErrorSerializer

from .models import Article
from .paginator import ArticlesPagination
from .serializers import ArticleSerializer, ArticlesLSerializer


class ArticlesL(generics.ListAPIView):
    queryset = Article.objects.filter(is_active=True)
    serializer_class = ArticlesLSerializer
    pagination_class = ArticlesPagination

    @swagger_auto_schema(operation_description="Get articles list",
                         responses={
                             status.HTTP_200_OK: ArticlesLSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ArticlesR(generics.RetrieveAPIView):
    queryset = Article.objects.filter(is_active=True)
    serializer_class = ArticleSerializer
    lookup_field = 'slug'
