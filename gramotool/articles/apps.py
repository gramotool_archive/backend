from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    name = 'gramotool.articles'
