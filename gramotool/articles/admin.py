from django.contrib import admin

from .models import Article, ArticleImage


class ArticleImageInline(admin.StackedInline):
    model = ArticleImage
    extra = 0
    fields = ['image']


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ["title", "is_active", "created_at"]
    inlines = [ArticleImageInline]
