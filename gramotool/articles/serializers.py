from django.conf import settings
from django.utils import dateformat
from rest_framework import serializers

from .models import Article


class ArticlesLSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = ['created_at', 'slug', 'title', 'cover']

    def to_representation(self, instance):
        representation = super(ArticlesLSerializer, self).to_representation(instance)
        representation['created_at'] = dateformat.format(instance.created_at, settings.DATE_FORMAT)
        return representation


class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = ['created_at', 'slug', 'title', 'content']

    def to_representation(self, instance):
        representation = super(ArticleSerializer, self).to_representation(instance)
        representation['created_at'] = dateformat.format(instance.created_at, settings.DATE_FORMAT)
        return representation
