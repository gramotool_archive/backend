from django.urls import path

from .views import ArticlesL, ArticlesR

urlpatterns = [
    path('', ArticlesL.as_view(), name='articles-list'),
    path('<str:slug>/', ArticlesR.as_view(), name='article-detail'),
]
