import hashlib
import hmac
import json
import random
import typing as t
from datetime import datetime

import requests
import six.moves.urllib as urllib
from pydantic import ValidationError
from requests import Response
from requests.cookies import cookiejar_from_dict
from requests.utils import dict_from_cookiejar

from gramotool.services.instagram_api.serializers.igtv import (
    InstagramIGTV, make_igtv_from_raw)

from .config import IG_SIG_KEY, REQUEST_HEADERS, SIG_KEY_VERSION, user_agent
from .errors import (AccountDelayError, AccountDisabledError,
                     AccountNotFoundError, ChallengeCodeSend,
                     ClientChallengeError, ClientLoginError,
                     ClientLoginRequiredError, LoginRequiredError)
from .serializers.feed import InstagramUserFeed, make_feed_from_raw
from .serializers.highlights import (InstagramUserHighlight,
                                     make_highlights_from_raw)
from .serializers.search import InstagramSearch
from .serializers.stories import (InstagramUserStory, make_hl_stories_from_raw,
                                  make_stories_from_raw)
from .serializers.user import InstagramUserInfo, make_user_info_from_raw
from .signing import generate_device_id, generate_uuid, hex_digest


class InstagramApiBadResponseDataError(Exception):
    pass


class ApiClient:
    API_URL = 'https://i.instagram.com/api/v1/'

    def __init__(self, username, password, proxy, ad_id=None, auth_settings=None,
                 device_id=None, uuid=None, phone_id=None, signature=None, csrftoken=None, **kwargs):
        self.username = username
        self.password = password
        self.auth_settings = auth_settings
        self.proxy = proxy

        self.device_id = device_id
        self.uuid = uuid
        self.ad_id = ad_id
        self.phone_id = phone_id
        self.signature = signature
        self.csrftoken = csrftoken
        self.key_version = SIG_KEY_VERSION

        self.session = requests.Session()

        self.timeout = kwargs.pop('timeout', 5)

    @property
    def timezone_offset(self) -> int:
        """Timezone offset in seconds. For use in certain functions."""
        return int(round((datetime.now() - datetime.utcnow()).total_seconds()))

    def get_from_cookie(self, key: str) -> t.Any:
        return self.auth_settings['cookies'][key]

    def get_default_headers(self) -> t.Dict:
        return {
            'User-Agent': user_agent,
            'X-Ads-Opt-Out': '0',
            'X-CM-Bandwidth-KBPS': '-1.000',
            'X-CM-Latency': '-1.000',
            'X-IG-App-Locale': 'en_US',
            'X-IG-Device-Locale': 'en_US',
            'X-IG-Connection-Speed': f'{random.randint(1000, 3700)}kbps',
            'X-IG-Bandwidth-Speed-KBPS': '-1.000',
            'X-IG-Bandwidth-TotalBytes-B': '0',
            'X-IG-Bandwidth-TotalTime-MS': '0',
            'X-IG-Extended-CDN-Thumbnail-Cache-Busting-Value': '1000',
            'X-Bloks-Version-Id': '1b030ce63a06c25f3e4de6aaaf6802fe1e76401bc5ab6e5fb85ed6c2d333e0c7',
            'X-MID': self.get_from_cookie('mid'),
            'X-IG-WWW-Claim': '0',
            'X-Bloks-Is-Layout-RTL': '1000',
            'X-IG-Connection-Type': 'WIFI',
            'X-IG-Capabilities': '3brTvwE=',
            'X-IG-App-ID': '567067343352427',
            'X-IG-Device-ID': self.uuid,
            'X-IG-Android-ID': self.device_id,
            'Accept-Language': 'en-US',
            'X-FB-HTTP-Engine': 'Liger',
            'Host': 'i.instagram.com',
            'Accept-Encoding': 'gzip',
            'Connection': 'close',
        }

    def generate_signature(self) -> None:
        self.phone_id = generate_uuid()
        self.uuid = generate_uuid()
        self.ad_id = generate_uuid()
        self.device_id = generate_device_id(hex_digest(self.username, self.password))

        data = json.dumps({
            'phone_id': self.phone_id,
            'device_id': self.device_id,
            'guid': self.uuid,
            'username': self.username,
            'password': self.password,
        })
        body = hmac.new(IG_SIG_KEY.encode('utf-8'), data.encode('utf-8'),
                        hashlib.sha256).hexdigest() + '.' + urllib.parse.quote(data)
        signature = f'ig_sig_key_version=4&signed_body={body}'
        self.signature = signature.format(body=body)

    def generate_data_signature(self, data: t.Any) -> hmac.HMAC:
        """
        Generates the signature for a data string

        :param data: content to be signed
        :return:
        """
        return hmac.new(
            self.signature.encode('ascii'), data.encode('ascii'),
            digestmod=hashlib.sha256).hexdigest()

    def parse_auth_settings(self) -> None:
        auth_settings = self.auth_settings

        self.device_id = auth_settings['device_id']
        self.uuid = auth_settings['uuid']
        self.ad_id = auth_settings['ad_id']
        self.phone_id = auth_settings['phone_id']
        self.signature = auth_settings['signature']
        self.session.headers.update(self.auth_settings['headers'])
        self.session.cookies = cookiejar_from_dict(self.auth_settings['cookies'])

    def get_csrftoken(self) -> None:
        session = requests.Session()
        session.headers.update(REQUEST_HEADERS)
        session.headers.update({'User-Agent': user_agent})
        get_params = {'challenge_type': 'signup', 'guid': self.uuid}
        prelogin_params = session.get(self.API_URL + 'si/fetch_headers/',
                                      proxies={'https': self.proxy.value},
                                      params=get_params, timeout=self.timeout)
        self.csrftoken = prelogin_params.cookies['csrftoken']
        self.session.cookies = prelogin_params.cookies

    def challenge_send_code(self, challenge_url: str, code: str) -> None:
        print(f'CHALLENGE SEND CODE: {code}')
        self.parse_auth_settings()
        self.get_csrftoken()

        post_params = {
            'security_code': code,
            'device_id': self.device_id,
            'guid': self.uuid,
            '_csrftoken': self.csrftoken,
        }

        self.session.post(challenge_url, data=post_params, proxies={'https': self.proxy.value},
                          allow_redirects=True,
                          timeout=self.timeout)

        # TODO auth after success code sending

        # self.login(frod=False)

    def challenge_receive_code(self, challenge_url: str) -> None:
        print(f'CHALLENGE RECEIVE CODE: {challenge_url}')
        post_params = {
            'choice': 1,
            'device_id': self.device_id,
            'guid': self.uuid,
            '_csrftoken': self.session.cookies['csrftoken'],
        }

        response = self.session.post(challenge_url, data=post_params, proxies={'https': self.proxy.value},
                                     allow_redirects=True, timeout=self.timeout)

        challenge_settings = {
            'device_id': self.device_id,
            'uuid': self.uuid,
            'ad_id': self.ad_id,
            'phone_id': self.phone_id,
            'signature': self.signature,
            'cookies': dict_from_cookiejar(response.cookies),
            'headers': dict(self.session.headers)
        }
        raise ChallengeCodeSend('enter_code', challenge_settings, challenge_url)

    def login(self, frod=True) -> t.Dict:
        login_url = self.API_URL + 'accounts/login/'

        if frod:
            self.generate_signature()
            self.session.headers.update(REQUEST_HEADERS)
            self.session.headers.update({'User-Agent': user_agent})

        post_params = self.signature

        response = self.session.post(login_url, data=post_params, proxies={'https': self.proxy.value},
                                     allow_redirects=True, timeout=self.timeout)

        self.session.cookies = response.cookies

        if response.status_code == 400:
            response_json = response.json()

            if response_json['error_type'] == 'inactive user':
                raise AccountDisabledError

            challenge_url = self.API_URL[:-1] + response.json()['challenge']['api_path']
            self.challenge_receive_code(challenge_url)

        if response.status_code == 200:
            auth_settings = {
                'device_id': self.device_id,
                'uuid': self.uuid,
                'ad_id': self.ad_id,
                'phone_id': self.phone_id,
                'signature': self.signature,
                'cookies': dict_from_cookiejar(response.cookies),
                'headers': dict(self.session.headers)
            }
            return auth_settings
        else:
            raise ClientLoginError

    def call_api(self, url: str, method: str = 'get', get_params=None, post_params=None) -> Response:
        request_url = self.API_URL + url

        if not self.auth_settings:
            raise ClientLoginRequiredError

        self.parse_auth_settings()
        self.session.headers.update(self.get_default_headers())

        try:
            print(f'call_api[{method}]: {request_url}')

            if method == 'get':
                response = self.session.get(request_url, params=get_params,
                                            proxies={'https': self.proxy.value},
                                            timeout=self.timeout)
            else:
                json_params = json.dumps(post_params, separators=(',', ':'))
                hash_sig = self.generate_data_signature(json_params)
                post_params = {
                    'ig_sig_key_version': self.key_version,
                    'signed_body': hash_sig + '.' + json_params
                }
                response = self.session.post(request_url, data=post_params,
                                             proxies={'https': self.proxy.value},
                                             timeout=self.timeout)

            if response.status_code == 404:
                raise AccountNotFoundError

            if response.status_code == 403:
                raise LoginRequiredError

            if response.status_code == 400:
                raise ClientChallengeError

            if response.status_code == 429:
                raise AccountDelayError(api_username=self.username)

            return response

        except TimeoutError:
            raise TimeoutError

    def get_account_stories(self, user_id: int) -> t.List[InstagramUserStory]:
        """ Get account story """
        api_url = f'feed/user/{user_id}/story/'
        data = self.call_api(api_url).json()

        try:
            return make_stories_from_raw(data)
        except ValidationError as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_info(self, username: str) -> InstagramUserInfo:
        """ Get account info """
        api_url = f'users/{username}/usernameinfo/'
        data = self.call_api(api_url).json()

        try:
            return make_user_info_from_raw(data['user'])
        except (KeyError, ValidationError) as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_highlights(self, user_id: int) -> t.List[InstagramUserHighlight]:
        """ Get account highlights """
        api_url = f'highlights/{user_id}/highlights_tray/'
        data = self.call_api(api_url).json()

        try:
            return make_highlights_from_raw(data)
        except (KeyError, ValidationError) as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_highlight_stories(self, highlight_id: str) -> t.List[InstagramUserStory]:
        """ Get account highlight stories """
        api_url = 'feed/reels_media/'
        post_params = {'user_ids': [highlight_id]}
        data = self.call_api(api_url, method='post', post_params=post_params).json()

        try:
            return make_hl_stories_from_raw(data, highlight_id=highlight_id)
        except ValidationError as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_feed(self, user_id: int, max_id) -> InstagramUserFeed:
        """
        Get the feed for the specified user id
        :param user_id:
        :param kwargs:
            - **max_id**: For pagination
            - **min_timestamp**: For pagination
        :return:
        """
        api_url = f'feed/user/{user_id}/'
        get_params = {
            'max_id': max_id,
        }
        data = self.call_api(api_url, method='get', get_params=get_params).json()

        try:
            return make_feed_from_raw(data)
        except (KeyError, ValidationError) as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_tags(self, user_id: int, max_id) -> InstagramUserFeed:
        """ Get account tags """
        api_url = f'usertags/{user_id}/feed/'
        get_params = {
            'max_id': max_id,
        }
        data = self.call_api(api_url, get_params=get_params).json()

        try:
            return make_feed_from_raw(data)
        except (KeyError, ValidationError) as err:
            raise InstagramApiBadResponseDataError(err)

    def get_account_igtv(self, user_id: int, max_id) -> InstagramIGTV:
        """ Get account igtv channel videos """
        api_url = 'igtv/channel/'
        post_params = {
            'id': f'user_{user_id}',
            'max_id': max_id,
        }
        data = self.call_api(api_url, post_params=post_params, method='post').json()

        try:
            return make_igtv_from_raw(data)
        except (KeyError, ValidationError) as err:
            raise InstagramApiBadResponseDataError(err)

    def search_accounts(self, search_query: str) -> InstagramSearch:
        """ Get accounts list by username """
        api_url = 'users/search/'
        get_params = {
            'q': search_query,
            'timezone_offset': self.timezone_offset,
            'count': 15,
        }
        data = self.call_api(api_url, method='get', get_params=get_params).json()

        try:
            return InstagramSearch(**data)
        except ValidationError as err:
            raise InstagramApiBadResponseDataError(err)
