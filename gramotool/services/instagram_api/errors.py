class ClientLoginError(BaseException):
    """Raised when login fails."""
    pass


class AccountDisabledError(BaseException):
    """Raised when account disabled."""
    pass


class AccountDelayError(BaseException):
    """Raised when instagram account has delay ban"""

    def __init__(self, api_username: str):
        self.api_username = api_username
        super(AccountDelayError, self).__init__()


class OnChallengeError(BaseException):
    """Raised when selected not a valid choice on challenge"""
    pass


class ChallengeCodeSend(BaseException):
    """Raised when challenge required."""

    def __init__(self, msg, challenge_settings=None, challenge_url=None):
        self.challenge_settings = challenge_settings
        self.challenge_url = challenge_url
        super(ChallengeCodeSend, self).__init__(msg)


class ClientChallengeError(BaseException):
    """Raised when challenge required."""
    pass


class LoginRequiredError(BaseException):
    """Raised when login required."""
    pass


class ClientLoginRequiredError(BaseException):
    """Raised when login is required."""
    pass


class AccountNotFoundError(BaseException):
    """Raised when instagram account not found"""
    pass


class AccountIsPrivateError(BaseException):
    """Raised when instagram account not found"""
    pass
