import hashlib
import uuid as uuid_library


def hex_digest(*args) -> str:
    m = hashlib.md5()
    m.update(b''.join([arg.encode('utf-8') for arg in args]))

    return m.hexdigest()


def generate_device_id(seed: str) -> str:
    volatile_seed = "12345"
    m = hashlib.md5()
    m.update(seed.encode('utf-8') + volatile_seed.encode('utf-8'))
    return 'android-' + m.hexdigest()[:16]


def generate_uuid() -> str:
    return str(uuid_library.uuid4())
