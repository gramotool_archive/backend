import typing as t

from pydantic import BaseModel, ValidationError
from pydantic import dataclasses as dc

__all__ = (
    'InstagramUserStory',
    'make_stories_from_raw',
)


class _Candidates(BaseModel):
    url: str


class _ImageVersions2(BaseModel):
    candidates: t.List[_Candidates]


class _VideoVersions(BaseModel):
    url: str


class _User(BaseModel):
    username: str


class _ReelMention(BaseModel):
    user: _User


class _Link(BaseModel):
    webUri: str


class _StoryCTA(BaseModel):
    links: t.Optional[t.List[_Link]] = None


class InstagramUserStoryRaw(BaseModel):
    pk: int
    media_type: int
    image_versions2: _ImageVersions2
    video_versions: t.Optional[t.List[_VideoVersions]]
    taken_at: str
    reel_mentions: t.Optional[t.List[_ReelMention]] = None
    story_cta: t.Optional[t.List[_StoryCTA]] = None


@dc.dataclass(frozen=True)
class InstagramUserStory:
    pk: int
    media_type: int
    media_cover: str
    taken_at: str
    media_src: str
    reel_mentions: t.Optional[t.List[str]] = None
    story_link: t.Optional[str] = None


def make_stories_from_raw(raw_data: t.Dict) -> t.List[InstagramUserStory]:
    try:
        stories = [] if raw_data['reel'] is None else raw_data['reel']['items']
    except KeyError as err:
        raise ValidationError(err)  # TODO parameter 'model' unfilled

    raw_stories = [InstagramUserStoryRaw(**story) for story in stories]
    return [make_story_from_raw(raw_story) for raw_story in raw_stories]


def make_hl_stories_from_raw(raw_data: t.Dict, highlight_id=None) -> t.List[InstagramUserStory]:
    try:
        stories = [] if len(raw_data['reels'].keys()) == 0 else raw_data['reels'][highlight_id]['items']
    except KeyError as err:
        raise ValidationError(err)  # TODO parameter 'model' unfilled

    raw_stories = [InstagramUserStoryRaw(**story) for story in stories]
    return [make_story_from_raw(raw_story) for raw_story in raw_stories]


def make_story_from_raw(raw_story: InstagramUserStoryRaw) -> InstagramUserStory:
    if raw_story.media_type == 1:  # photo
        media_src = raw_story.image_versions2.candidates[0].url
    else:
        media_src = raw_story.video_versions[0].url

    reel_mentions = [reel.user.username for reel in raw_story.reel_mentions] if raw_story.reel_mentions else None

    if raw_story.story_cta and raw_story.story_cta[0].links is not None:
        story_link = raw_story.story_cta[0].links[0].webUri
    else:
        story_link = None

    return InstagramUserStory(
                pk=raw_story.pk,
                media_type=raw_story.media_type,
                taken_at=raw_story.taken_at,
                media_cover=raw_story.image_versions2.candidates[-1].url,
                media_src=media_src,
                reel_mentions=reel_mentions,
                story_link=story_link,
            )
