import typing as t

from pydantic import BaseModel

from gramotool.services.instagram_api.serializers.user import \
    InstagramUserInfoSmall

__all__ = (
    'InstagramSearch',
)


class InstagramSearch(BaseModel):
    users: t.List[InstagramUserInfoSmall]
