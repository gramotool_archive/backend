import typing as t

from pydantic import BaseModel
from pydantic import dataclasses as dc

__all__ = (
    'make_igtv_from_raw',
)


class _VideoVersions(BaseModel):
    url: str


class _Candidates(BaseModel):
    url: str


class _ImageVersions2(BaseModel):
    candidates: t.List[_Candidates]


class _InstagramIGTVItem(BaseModel):
    pk: int
    code: str
    video_duration: float
    view_count: float
    comment_count: int
    title: str
    image_versions2: _ImageVersions2
    video_versions: t.List[_VideoVersions]

    def get_cover(self):
        return self.image_versions2.candidates[0].url

    def get_src(self):
        return self.video_versions[0].url


class InstagramIGTVRaw(BaseModel):
    items: t.List[_InstagramIGTVItem]
    more_available: bool
    max_id: t.Optional[str] = None


@dc.dataclass(frozen=True)
class InstagramIGTVItem:
    pk: int
    code: str
    video_duration: float
    view_count: float
    cover: str
    title: str
    src: str
    comment_count: int


@dc.dataclass(frozen=True)
class InstagramIGTV:
    items: t.List[InstagramIGTVItem]
    more_available: bool
    max_id: t.Optional[str] = None


def make_igtv_from_raw(raw_data: t.Dict) -> InstagramIGTV:
    raw_igtv = InstagramIGTVRaw(**raw_data)
    items = []

    for i in raw_igtv.items:
        igtv = InstagramIGTVItem(
            pk=i.pk,
            code=i.code,
            title=i.title,
            comment_count=i.comment_count,
            video_duration=i.video_duration,
            view_count=i.view_count,
            cover=i.get_cover(),
            src=i.get_src(),
        )
        items.append(igtv)

    igtv = InstagramIGTV(
        items=items,
        more_available=raw_igtv.more_available,
        max_id=raw_igtv.max_id
    )

    return igtv
