import typing as t

from pydantic import BaseModel

__all__ = (
    'InstagramUserHighlight',
    'make_highlights_from_raw',
)


class _CroppedImageVersions(BaseModel):
    url: str


class _CoverMedia(BaseModel):
    cropped_image_version: _CroppedImageVersions


class _Tray(BaseModel):
    id: str
    title: str
    media_count: int
    cover_media: _CoverMedia

    @property
    def clear_id(self):
        return self.id.split(':')[1]


class InstagramUserHighlightsRaw(BaseModel):
    tray: t.List[_Tray]


class InstagramUserHighlight(BaseModel):
    id: str
    title: str
    media_count: int
    cover: str


def make_highlights_from_raw(raw_data: t.Dict) -> t.List[InstagramUserHighlight]:
    result = []
    raw_highlights = InstagramUserHighlightsRaw(**raw_data)

    for raw_highlight in raw_highlights.tray:
        cover = raw_highlight.cover_media.cropped_image_version.url

        highlight = InstagramUserHighlight(
            id=raw_highlight.clear_id,
            title=raw_highlight.title,
            media_count=raw_highlight.media_count,
            cover=cover
        )
        result.append(highlight)

    return result
