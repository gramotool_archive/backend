import typing as t

from pydantic import BaseModel
from pydantic import dataclasses as dc

__all__ = (
    'InstagramUserFeed',
    'make_feed_from_raw',
)


class _Candidates(BaseModel):
    url: str


class _ImageVersions2(BaseModel):
    candidates: t.List[_Candidates]


class _VideoVersions(BaseModel):
    url: str


class _CarouselMediaItem(BaseModel):
    pk: int
    id: str
    media_type: int
    image_versions2: _ImageVersions2
    video_versions: t.Optional[t.List[_VideoVersions]] = None


class _Caption(BaseModel):
    text: str


class _Items(BaseModel):
    pk: int
    id: str
    code: str
    media_type: int
    like_count: int
    comment_count: int
    taken_at: int

    view_count: t.Optional[int] = None
    usertags: t.Optional[t.Dict] = None
    caption: t.Optional[_Caption] = None

    image_versions2: t.Optional[_ImageVersions2] = None
    video_versions: t.Optional[t.List[_VideoVersions]] = None

    carousel_media_count: t.Optional[int] = None
    carousel_media: t.Optional[t.List[_CarouselMediaItem]] = None


class InstagramUserFeedRaw(BaseModel):
    items: t.List[_Items]
    num_results: int
    more_available: bool
    next_max_id: t.Optional[str] = None


@dc.dataclass(frozen=True)
class InstagramUserFeedCarouselMedia:
    pk: int
    id: str
    media_type: int
    cover: str
    src: str


@dc.dataclass(frozen=True)
class InstagramUserFeedItem:
    pk: int
    id: str
    code: str
    media_type: int
    like_count: int
    taken_at: int
    comment_count: int

    view_count: t.Optional[int] = None
    caption: t.Optional[str] = None
    usertags: t.Optional[t.List[str]] = None
    cover: t.Optional[str] = None
    src: t.Optional[str] = None
    carousel_media: t.Optional[t.List[InstagramUserFeedCarouselMedia]] = None


@dc.dataclass(frozen=True)
class InstagramUserFeed:
    items: t.List[InstagramUserFeedItem]
    num_results: int
    more_available: bool
    next_max_id: t.Optional[str] = None


def make_feed_from_raw(raw_data: t.Dict) -> InstagramUserFeed:
    feed_items = []
    caption, usertags = None, None

    raw_feed = InstagramUserFeedRaw(**raw_data)

    for item in raw_feed.items:

        if item.caption:
            caption = item.caption.text

        if item.usertags:
            usertags = [item['user']['username'] for item in item.usertags['in']]

        if item.media_type == 1 or item.media_type == 2:  # photo or video

            cover = item.image_versions2.candidates[-1].url

            if item.media_type == 1:
                src = item.image_versions2.candidates[0].url
            else:
                src = item.video_versions[0].url

            feed_item = InstagramUserFeedItem(
                pk=item.pk,
                id=item.id,
                code=item.code,
                taken_at=item.taken_at,
                media_type=item.media_type,
                like_count=item.like_count,
                view_count=item.view_count,
                caption=caption,
                usertags=usertags,
                comment_count=item.comment_count,
                cover=cover,
                src=src
            )

        else:  # carousel
            feed_item = InstagramUserFeedItem(
                pk=item.pk,
                id=item.id,
                code=item.code,
                taken_at=item.taken_at,
                media_type=item.media_type,
                like_count=item.like_count,
                caption=caption,
                usertags=usertags,
                comment_count=item.comment_count,
                carousel_media=get_carousel_medias_list(item.carousel_media)
            )

        feed_items.append(feed_item)

    result = InstagramUserFeed(
        num_results=raw_feed.num_results,
        more_available=raw_feed.more_available,
        next_max_id=raw_feed.next_max_id,
        items=feed_items
    )
    return result


def get_carousel_medias_list(raw_data: t.List[_CarouselMediaItem]) -> t.List[InstagramUserFeedCarouselMedia]:
    result = []

    for item in raw_data:
        cover = item.image_versions2.candidates[-1].url

        if item.media_type == 1:
            src = item.image_versions2.candidates[0].url
        else:
            src = item.video_versions[0].url

        media_item = InstagramUserFeedCarouselMedia(
            pk=item.pk,
            id=item.id,
            media_type=item.media_type,
            cover=cover,
            src=src,
        )
        result.append(media_item)

    return result
