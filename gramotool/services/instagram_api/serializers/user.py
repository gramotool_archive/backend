import typing as t

from pydantic import BaseModel
from pydantic import dataclasses as dc

__all__ = (
    'InstagramUserInfo',
    'InstagramUserInfoSmall',
    'make_user_info_from_raw',
)


class _HDProfilePicUrlInfo(BaseModel):
    url: str


class InstagramUserInfoRaw(BaseModel):
    pk: int
    username: str
    full_name: str
    biography: str
    external_url: str

    follower_count: int
    following_count: int
    media_count: int
    total_igtv_videos: int
    usertags_count: int

    is_private: bool
    is_verified: bool
    is_business: bool

    profile_pic_id: t.Optional[str] = None
    public_email: t.Optional[str] = None
    contact_phone_number: t.Optional[str] = None

    profile_pic_url: str
    hd_profile_pic_url_info: t.Optional[_HDProfilePicUrlInfo] = None
    hd_profile_pic_versions: t.List[_HDProfilePicUrlInfo] = None


@dc.dataclass(frozen=True)
class HDProfilePicUrlInfo:
    url: str


@dc.dataclass(frozen=True)
class InstagramUserInfo:
    pk: int
    username: str
    full_name: str
    biography: str
    external_url: str

    follower_count: int
    following_count: int
    media_count: int
    total_igtv_videos: int
    usertags_count: int

    profile_pic_s_url: str
    profile_pic_url: str

    is_private: bool
    is_verified: bool
    is_business: bool

    public_email: t.Optional[str] = None
    contact_phone_number: t.Optional[str] = None


class InstagramUserInfoSmall(BaseModel):
    pk: int
    username: str
    full_name: str
    profile_pic_url: str
    is_private: bool
    is_verified: bool


def make_user_info_from_raw(raw_data: t.Dict) -> InstagramUserInfo:
    raw_info = InstagramUserInfoRaw(**raw_data)

    if raw_info.hd_profile_pic_url_info:
        profile_pic_url = raw_info.hd_profile_pic_url_info.url
    else:
        profile_pic_url = raw_info.hd_profile_pic_versions[-1].url

    return InstagramUserInfo(
        pk=raw_info.pk,
        username=raw_info.username,
        full_name=raw_info.full_name,
        biography=raw_info.biography,
        external_url=raw_info.external_url,
        follower_count=raw_info.follower_count,
        following_count=raw_info.following_count,
        media_count=raw_info.media_count,
        total_igtv_videos=raw_info.total_igtv_videos,
        usertags_count=raw_info.usertags_count,
        profile_pic_url=profile_pic_url,
        profile_pic_s_url=raw_info.profile_pic_url,
        is_private=raw_info.is_private,
        is_verified=raw_info.is_verified,
        is_business=raw_info.is_business,
        public_email=raw_info.public_email,
        contact_phone_number=raw_info.contact_phone_number,
    )
