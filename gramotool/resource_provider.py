from datetime import datetime, timedelta

from django_rq import get_scheduler
from rq_scheduler import Scheduler

from gramotool.iaccounts.services import InstagramAccountService
from gramotool.tools.jobs import RQJobs
from gramotool.tools.services_new.provider import ProviderService
from gramotool.tools.services_new.proxy import ProxyService
from gramotool.tools.services_new.working_account import WorkingAccountService
from gramotool.tools.use_cases.get_most_chilly_working_account import \
    GetMostChillyWorkingAccountUseCase
from gramotool.tools.use_cases.manage_wa_work_readiness import \
    ManageWAWorkReadinessUseCase
from gramotool.use_cases.get_metrics import GetMetricsUseCase


class ResourceProvider:
    working_account_service: WorkingAccountService
    provider_service: ProviderService
    proxy_service: ProxyService
    ia_service: InstagramAccountService

    get_metrics_use_case: GetMetricsUseCase
    get_most_chilly_working_account_use_case: GetMostChillyWorkingAccountUseCase
    manage_wa_work_readiness_use_case: ManageWAWorkReadinessUseCase

    def __init__(self) -> None:
        print('RP: Initializing')

        self._init_services()
        self._init_use_cases()
        self._init_jobs()

    def _init_services(self) -> None:
        self.working_account_service = WorkingAccountService()
        self.provider_service = ProviderService()
        self.proxy_service = ProxyService()
        self.ia_service = InstagramAccountService()

    def _init_use_cases(self) -> None:
        self.get_metrics_use_case = GetMetricsUseCase(
            wa_service=self.working_account_service,
        )
        self.get_most_chilly_working_account_use_case = GetMostChillyWorkingAccountUseCase(
            wa_service=self.working_account_service,
        )
        self.manage_wa_work_readiness_use_case = ManageWAWorkReadinessUseCase(
            wa_service=self.working_account_service,
        )

    def _init_jobs(self) -> None:
        print('RP: Configuring scheduler')

        scheduler: Scheduler = get_scheduler('schedule', interval=5)

        for s_job in scheduler.get_jobs():
            s_job.delete()

        jobs = RQJobs(provider_service=self.provider_service,
                      proxy_service=self.proxy_service,
                      wa_service=self.working_account_service,
                      manage_wa_work_readiness_use_case=self.manage_wa_work_readiness_use_case,
                      ia_service=self.ia_service)

        scheduler.schedule(
            scheduled_time=datetime.utcnow() + timedelta(seconds=10),
            func=jobs.check_disabled_proxies,
            interval=60,
        )

        scheduler.schedule(
            scheduled_time=datetime.utcnow() + timedelta(seconds=5),
            func=jobs.check_providers,
            interval=30,
        )

        scheduler.schedule(
            scheduled_time=datetime.utcnow() + timedelta(seconds=5),
            func=jobs.reauth_challenge_wa,
            interval=30,
        )

        scheduler.schedule(
            scheduled_time=datetime.utcnow(),
            func=jobs.manage_wa_work_readiness,
            interval=5,
        )

        scheduler.schedule(
            scheduled_time=datetime.utcnow() + timedelta(seconds=10),
            func=jobs.manage_accounts_media,
            interval=60,
        )


rp = ResourceProvider()
