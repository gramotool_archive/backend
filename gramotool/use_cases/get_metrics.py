from prometheus_client import (CollectorRegistry, Gauge, generate_latest,
                               multiprocess)

from gramotool.tools.services_new.working_account import WorkingAccountService

__all__ = (
    'Metrics',
    'GetMetricsUseCase',
)

Metrics = str


class GetMetricsUseCase:
    working_accounts: Gauge

    def __init__(self,
                 wa_service: WorkingAccountService,
                 ) -> None:
        self.wa_service = wa_service

    def _init_metrics(self, registry: CollectorRegistry) -> None:
        self.working_accounts = Gauge(
            'working_accounts',
            'The amount of work accounts',
            ['status'],
            registry=registry,
        )

    def run(self) -> Metrics:
        registry = CollectorRegistry()
        multiprocess.MultiProcessCollector(registry)

        self._init_metrics(registry)
        self._set_working_account_metrics()

        metrics = generate_latest(registry=registry)
        return Metrics(metrics.decode('utf-8'))

    def _set_working_account_metrics(self) -> None:
        active_wa = self.wa_service.get_active().count()
        is_ready_to_work = self.wa_service.get_ready_to_work().count()

        self.working_accounts.labels('is_active').set(active_wa)
        self.working_accounts.labels('is_ready_to_work').set(is_ready_to_work)
