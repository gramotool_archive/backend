from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'gramotool.users'

    verbose_name = "Аккаунты"

    def ready(self):
        import gramotool.users.signals  # noqa F401
