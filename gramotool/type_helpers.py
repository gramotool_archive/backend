from typing import Generic, Iterator, TypeVar

from django.db.models import QuerySet

T = TypeVar("T")


class Query(Generic[T], QuerySet):
    def __iter__(self) -> Iterator[T]: ...
