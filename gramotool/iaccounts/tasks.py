from django_rq import job

from gramotool.iaccounts.services import InstagramAccountService
from gramotool.services.instagram_api.serializers.user import InstagramUserInfo

from .models.accounts import (AccountHistory, AccountLinkHistory,
                              InstagramAccount)


@job
def update_account_info(account: InstagramUserInfo) -> None:
    prefix = f'[D][update_account_info][username:{account.username}]'
    print(prefix)

    update_dict = {
        'account_id': account.pk,
        'username': account.username,
        'followers': account.follower_count,
        'following': account.following_count,
        'media_count': account.media_count,
        'profile_pic_s': account.profile_pic_s_url,
        'profile_pic': account.profile_pic_url,
        'full_name': account.full_name,
        'is_private': account.is_private,
        'is_verified': account.is_verified,
        'is_business': account.is_business,
    }

    if account.is_business:
        update_dict['public_email'] = account.public_email
        update_dict['public_phone_number'] = account.contact_phone_number

    InstagramAccount.objects.update_or_create(account_id=account.pk, defaults=update_dict)


@job('high')
def parse_account_external_url(account: InstagramUserInfo) -> None:
    prefix = f'[D][parse_account_external_url][username:{account.username}]'
    print(prefix)

    iaccount = InstagramAccount.objects.only('id').get(account_id=account.pk)

    if account.external_url:
        account_history, created = AccountHistory.objects.get_or_create(iaccount=iaccount)

        if not AccountLinkHistory.objects\
                .filter(account_history__iaccount__account_id=account.pk, url=account.external_url)\
                .exists():
            AccountLinkHistory.objects.create(url=account.external_url, account_history=account_history)


@job('high')
def parse_account_media(username: str) -> None:
    from gramotool.tools.services import InstagramTools

    prefix = f'[D][parse_account_media][username:{username}]'
    print(prefix)

    feed = InstagramTools().get_account_feed(username, None)
    InstagramAccountService().save_media_from_feed(feed, username)
