from django.contrib import admin
from django.utils.safestring import mark_safe

from .models.accounts import (AccountHistory, AccountLinkHistory, AccountMedia,
                              AccountMediaItem, AccountTag, InstagramAccount)


class AccountMediaItemInline(admin.StackedInline):
    model = AccountMediaItem
    extra = 0
    fields = ['src', 'cover', 'media_id', 'created_at']
    readonly_fields = ['created_at']


@admin.register(AccountMedia)
class AccountMediaAdmin(admin.ModelAdmin):
    model = AccountMedia
    list_display = ['iaccount', 'is_need_to_parse', 'get_items_count']
    readonly_fields = ['updated_at']
    raw_id_fields = ["iaccount"]
    fields = ['iaccount', 'updated_at', 'is_need_to_parse']
    inlines = [AccountMediaItemInline]

    def get_items_count(self, instance: AccountMedia):
        return instance.items.count()


@admin.register(InstagramAccount)
class InstagramAccountAdmin(admin.ModelAdmin):
    list_display = ['username', 'account_id', 'followers', 'following', 'is_monitoring',
                    'is_private', 'avatar_pic', 'instagram_link']
    search_fields = ['username', 'account_id']
    readonly_fields = ['full_name', 'public_email', 'public_phone_number']
    list_filter = ['is_monitoring']
    list_per_page = 10

    fieldsets = (
        ('Данные', {
            'fields': (
                ('is_monitoring', 'is_private', 'is_verified', 'is_business',),
                ('account_id', 'username', 'full_name',),
                ('public_email', 'public_phone_number',),
                ('followers', 'following',),
                ('profile_pic',),
                ('profile_pic_s',),
            )
        }),
    )

    def avatar_pic(self, obj):
        if obj.profile_pic:
            return mark_safe(
                f'<a href="{obj.profile_pic}" target="_blank">'
                f'<img src="{obj.profile_pic}" height="50" width="50" style="border-radius:50px;"/></a>'
            )
        else:
            return '(Нет изображения)'

    avatar_pic.short_description = 'Аватарка'

    def instagram_link(self, obj):
        return mark_safe('<a href="https://instagram.com/{0}" target="_blank">Instagram</a>'.format(obj.username))


@admin.register(AccountTag)
class AccountTagAdmin(admin.ModelAdmin):
    list_display = ["title"]
    search_fields = ["title"]


class AccountLinkHistoryInline(admin.StackedInline):
    list_display = ['url', 'account_history', 'created_at']
    readonly_fields = ['account_history']
    model = AccountLinkHistory
    extra = 0


@admin.register(AccountHistory)
class AccountHistoryAdmin(admin.ModelAdmin):
    list_display = ['iaccount']
    readonly_fields = ['iaccount']
    inlines = [AccountLinkHistoryInline]
