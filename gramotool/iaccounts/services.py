import typing as t

from gramotool.iaccounts.models.accounts import (AccountMedia,
                                                 AccountMediaItem,
                                                 InstagramAccount)
from gramotool.services.instagram_api.serializers.feed import InstagramUserFeed


class InstagramAccountService:
    model = InstagramAccount

    @staticmethod
    def save_media_from_feed(feed: InstagramUserFeed, username: str) -> None:
        account_media = AccountMedia.objects.get(iaccount__username=username)
        media_items = feed.items[:6]

        for item in media_items:
            if not AccountMediaItem.objects.filter(media_id=item.id).exists():
                cover = item.cover
                src = item.src

                if item.media_type == 8:
                    cover = item.carousel_media[0].cover
                    src = item.carousel_media[0].src

                AccountMediaItem.objects.create(
                    iaccount_media=account_media,
                    src=src,
                    media_id=item.id,
                    cover=cover,
                )

        if account_media.items.count() == len(media_items):
            account_media.is_need_to_parse = False
            account_media.save(update_fields=['is_need_to_parse'])

    def get_accounts_username_to_parse(self) -> t.List[str]:
        return self.model.objects.filter(media__is_need_to_parse=True).values_list('username', flat=True)

    def parse_media(self) -> None:
        from gramotool.iaccounts.tasks import parse_account_media

        accounts_to_parse = self.get_accounts_username_to_parse()

        for username in accounts_to_parse:
            parse_account_media.delay(username)
