from rest_framework import serializers

from .models.accounts import (AccountLinkHistory, AccountMedia,
                              AccountMediaItem, AccountTag, InstagramAccount)


class AccountTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountTag
        fields = ['title']


class InstagramAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstagramAccount
        fields = ['account_id', 'profile_pic_s', 'username', 'followers', 'following', 'media_count']


class AccountLinkHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountLinkHistory
        fields = ['url']


class AccountMediaItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountMediaItem
        fields = ['src', 'cover']


class AccountMediaSerializer(serializers.ModelSerializer):
    items = AccountMediaItemSerializer(many=True)

    class Meta:
        model = AccountMedia
        fields = ['items']


class CompilationAccountSerializer(serializers.ModelSerializer):
    media = AccountMediaSerializer()

    class Meta:
        model = InstagramAccount
        fields = ['account_id', 'profile_pic_s', 'username', 'followers', 'following', 'media_count', 'media']
