from django.apps import AppConfig


class IaccountsConfig(AppConfig):
    name = 'gramotool.iaccounts'
