from datetime import datetime

from django.db import models


class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True

    def mark_last_updated(self):
        self.updated_at = datetime.now()
        self.save()
