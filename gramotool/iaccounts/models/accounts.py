from django.db import models

from .base import Base


class InstagramAccount(Base):
    account_id = models.CharField(max_length=15, db_index=True)
    username = models.CharField(max_length=30, db_index=True, verbose_name='Имя пользователя')
    full_name = models.CharField(max_length=100, null=True, blank=True, verbose_name='Имя')

    followers = models.PositiveIntegerField(null=True, blank=True, verbose_name='Подписчики')
    following = models.PositiveIntegerField(null=True, blank=True, verbose_name='Подписки')
    media_count = models.PositiveIntegerField(null=True, blank=True, verbose_name='Количество постов')

    profile_pic = models.URLField(max_length=300, null=True, blank=True, verbose_name='Аватарка')
    profile_pic_s = models.URLField(max_length=300, null=True, blank=True, verbose_name='Аватарка s')
    profile_pic_id = models.CharField(max_length=40, null=True, blank=True)

    is_monitoring = models.BooleanField(default=False, verbose_name='Отслеживать')
    is_private = models.BooleanField(default=False, verbose_name='Приватный аккаунт')
    is_verified = models.BooleanField(default=False, verbose_name='Верифицированный аккаунт')
    is_business = models.BooleanField(default=False, verbose_name='Бизнес профиль')

    public_email = models.EmailField(null=True, blank=True, verbose_name='Публичный Email')
    public_phone_number = models.CharField(max_length=20, null=True, blank=True, verbose_name='Публичный телефон')

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'


class AccountTag(models.Model):
    title = models.CharField('Название', max_length=50)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Аккаунт / Тэг'
        verbose_name_plural = 'Аккаунт / Тэги'


class AccountHistory(Base):
    iaccount = models.OneToOneField(InstagramAccount, on_delete=models.CASCADE, related_name='history',
                                    verbose_name='Аккаунт')

    class Meta:
        verbose_name = 'Аккаунт / История'
        verbose_name_plural = 'Аккаунт / История'


class AccountLinkHistory(Base):
    account_history = models.ForeignKey(AccountHistory, null=True, on_delete=models.CASCADE, verbose_name='Аккаунт')
    url = models.URLField(verbose_name='Ссылка')

    def __str__(self):
        return self.url

    class Meta:
        verbose_name = 'Аккаунт / История / Ссылка профиля'
        verbose_name_plural = 'Аккаунт / История / Ссылки профиля'
        ordering = ['-created_at']


class AccountMedia(Base):
    iaccount = models.OneToOneField(InstagramAccount, on_delete=models.CASCADE, related_name='media')

    is_need_to_parse = models.BooleanField(default=True)

    def __str__(self):
        return self.iaccount.username

    class Meta:
        verbose_name = 'Аккаунт / Публикация'
        verbose_name_plural = 'Аккаунт / Публикации'


class AccountMediaItem(Base):
    iaccount_media = models.ForeignKey(AccountMedia, related_name='items', on_delete=models.CASCADE)
    src = models.URLField(max_length=700)
    cover = models.URLField(max_length=700)
    media_id = models.CharField(max_length=50)

    class Meta:
        ordering = ['-created_at']
