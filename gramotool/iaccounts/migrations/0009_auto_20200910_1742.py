# Generated by Django 3.0.8 on 2020-09-10 17:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iaccounts', '0008_auto_20200910_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountmediaitem',
            name='cover',
            field=models.URLField(max_length=700),
        ),
        migrations.AlterField(
            model_name='accountmediaitem',
            name='iaccount_media',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='iaccounts.AccountMedia'),
        ),
        migrations.AlterField(
            model_name='accountmediaitem',
            name='src',
            field=models.URLField(max_length=300),
        ),
    ]
