# Generated by Django 3.0.8 on 2020-09-10 17:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iaccounts', '0009_auto_20200910_1742'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountmediaitem',
            name='src',
            field=models.URLField(max_length=600),
        ),
    ]
