import dataclasses as dc
import typing as t

from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework_dataclasses.serializers import DataclassSerializer

__all__ = (
    'Error',
    'make_response',
    'make_error_response',
)


@dc.dataclass(frozen=True)
class Error:
    message: str
    type: t.Optional[str] = None
    code: t.Optional[str] = None


@dc.dataclass(frozen=True)
class ResponseBodyWithError:
    error: Error


class ErrorSerializer(DataclassSerializer):
    class Meta:
        dataclass = ResponseBodyWithError


def make_response(serializer: t.Type[Serializer],
                  data: t.Any,
                  status_code: t.Optional[int] = status.HTTP_200_OK) -> Response:
    return Response(serializer(instance={'data': data}).data, status=status_code)


def make_error_response(error: Error,
                        status_code: t.Optional[int] = status.HTTP_400_BAD_REQUEST) -> Response:
    return Response(ErrorSerializer(instance={'error': error}).data, status=status_code)
