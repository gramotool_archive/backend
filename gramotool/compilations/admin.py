from django.contrib import admin

from .models import Compilation


@admin.register(Compilation)
class CompilationAdmin(admin.ModelAdmin):
    list_display = ["title", "is_active"]
    search_fields = ["title"]
    list_filter = ["is_active"]
    list_editable = ["is_active"]
    raw_id_fields = ["iaccounts"]
