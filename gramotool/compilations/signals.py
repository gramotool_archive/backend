from django.db.models.signals import post_save
from django.dispatch import receiver

from gramotool.compilations.models import Compilation
from gramotool.iaccounts.models.accounts import AccountMedia


@receiver(post_save, sender=Compilation)
def post_save_compilation(instance: Compilation, created: bool, **kwargs) -> None:
    accounts_without_media = instance.iaccounts.filter(media__isnull=True)

    for account in accounts_without_media:
        AccountMedia.objects.create(iaccount=account)
