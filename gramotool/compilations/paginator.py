from rest_framework.pagination import PageNumberPagination


class CompilationsPagination(PageNumberPagination):
    page_size = 6
