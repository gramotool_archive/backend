from django.apps import AppConfig


class CompilationsConfig(AppConfig):
    name = 'gramotool.compilations'
    verbose_name = 'Подборки'

    def ready(self) -> None:
        from gramotool.compilations import signals  # noqa F401
