from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.generics import ListAPIView, get_object_or_404

from gramotool.compilations.paginator import CompilationsPagination
from gramotool.http_response_helpers import ErrorSerializer
from gramotool.iaccounts.models.accounts import AccountTag, InstagramAccount
from gramotool.iaccounts.serializers import AccountTagSerializer

from .models import Compilation
from .serializers import (CompilationDetailSerializer,
                          CompilationsListSerializer,
                          CompilationSmallSerializer)

tags_param = openapi.Parameter('tags', openapi.IN_QUERY, description="for search by tags", type=openapi.TYPE_STRING)
query_param = openapi.Parameter('q', openapi.IN_QUERY, description="for search by query", type=openapi.TYPE_STRING)


class CompilationsL(generics.ListAPIView):
    serializer_class = CompilationsListSerializer
    pagination_class = CompilationsPagination

    @swagger_auto_schema(operation_description="Get compilations list",
                         responses={
                             status.HTTP_200_OK: CompilationsListSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         },
                         manual_parameters=[tags_param, query_param])
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        """
        Optionally restricts the returned compilations,
        by filtering against a `tags` query parameter in the URL.
        """
        queryset = Compilation.objects.filter(is_active=True)

        tags = self.request.query_params.get('tags', None)
        if tags is not None:
            tags = tags.split(',')
            queryset = queryset.filter(tags__title__in=tags).distinct()

        search_query = self.request.query_params.get('q', None)
        if search_query is not None:
            queryset = queryset.filter(title__icontains=search_query).distinct()

        return queryset


class CompilationR(generics.RetrieveAPIView):
    queryset = Compilation.objects.filter(is_active=True)
    serializer_class = CompilationDetailSerializer
    lookup_field = 'slug'


class CompilationSimilar(generics.ListAPIView):
    queryset = Compilation.objects.filter(is_active=True)
    serializer_class = CompilationSmallSerializer
    pagination_class = CompilationsPagination

    def get_queryset(self):
        similar_compilation_slug = self.kwargs.get('slug')
        similar_compilation = Compilation.objects.get(slug=similar_compilation_slug, is_active=True)
        similar_compilations_tags = similar_compilation.tags.values_list('id', flat=True)

        queryset = Compilation.objects\
            .filter(tags__in=list(similar_compilations_tags), is_active=True)\
            .exclude(id=similar_compilation.id)

        return queryset


class CompilationsTags(generics.ListAPIView):
    queryset = AccountTag.objects.all()
    serializer_class = AccountTagSerializer


class CompilationsByUsernameL(ListAPIView):
    serializer_class = CompilationSmallSerializer

    @swagger_auto_schema(operation_description="Get collections with specific account included",
                         responses={
                             status.HTTP_200_OK: CompilationSmallSerializer,
                             status.HTTP_400_BAD_REQUEST: ErrorSerializer(),
                         })
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        account_username = self.kwargs.get('username', None)
        iaccount = get_object_or_404(InstagramAccount, username=account_username)
        queryset = Compilation.objects.filter(iaccounts__in=[iaccount], is_active=True)[:3]
        return queryset
