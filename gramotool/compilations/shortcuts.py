import uuid


def get_compilations_upload_path(self, filename):
    ext = filename.split('.')[-1]
    filename = f"{uuid.uuid4()}.{ext}"
    return f'compilations/{self.id}/{filename}'
