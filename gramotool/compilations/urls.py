from django.urls import path

from .views import (CompilationR, CompilationsByUsernameL, CompilationSimilar,
                    CompilationsL, CompilationsTags)

urlpatterns = [
    path('', CompilationsL.as_view(), name='compilations-list'),
    path('tags/', CompilationsTags.as_view(), name='compilations-tags-list'),
    path('<str:slug>/', CompilationR.as_view(), name='compilation-data'),
    path('<str:slug>/similar/', CompilationSimilar.as_view(), name='compilation-get-similar'),
    path('account/<str:username>/', CompilationsByUsernameL.as_view(), name='get-collections-by-username'),
]
