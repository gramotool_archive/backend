from django.conf import settings
from django.utils import dateformat
from rest_framework import serializers

from gramotool.iaccounts.serializers import (AccountTagSerializer,
                                             CompilationAccountSerializer,
                                             InstagramAccountSerializer)

from .models import Compilation


class CompilationDetailSerializer(serializers.ModelSerializer):
    tags = AccountTagSerializer(many=True)
    iaccounts = CompilationAccountSerializer(many=True)

    class Meta:
        model = Compilation
        fields = ['created_at', 'slug', 'title', 'description', 'cover', 'tags', 'iaccounts']

    def to_representation(self, instance):
        representation = super(CompilationDetailSerializer, self).to_representation(instance)
        representation['created_at'] = dateformat.format(instance.created_at, settings.DATE_FORMAT)
        return representation


class CompilationsListSerializer(serializers.ModelSerializer):
    tags = AccountTagSerializer(many=True)
    iaccounts = InstagramAccountSerializer(many=True)

    class Meta:
        model = Compilation
        fields = ['created_at', 'slug', 'title', 'description', 'cover', 'tags', 'iaccounts']

    def to_representation(self, instance):
        representation = super(CompilationsListSerializer, self).to_representation(instance)
        representation['created_at'] = dateformat.format(instance.created_at, settings.DATE_FORMAT)
        return representation


class CompilationSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = Compilation
        fields = ['slug', 'title']
