import random
import uuid

from django.db import models
from pytils.translit import slugify

from gramotool.iaccounts.models.accounts import AccountTag, InstagramAccount

from .shortcuts import get_compilations_upload_path


class Compilation(models.Model):
    unique_id = models.UUIDField(default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField('Заголовок', max_length=70)
    slug = models.SlugField(max_length=100, blank=True)
    description = models.TextField('Описание', null=True, blank=True)
    cover = models.ImageField('Обложка', upload_to=get_compilations_upload_path, null=True, blank=True)
    tags = models.ManyToManyField(AccountTag, related_name='compilations', verbose_name='Тэги')
    iaccounts = models.ManyToManyField(InstagramAccount, related_name='compilations', verbose_name='Аккаунты')

    is_accounts_media_parsed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = slugify(self.title)
            if Compilation.objects.filter(slug=slug).exists():
                slug = self.generate_unique_slug(base_slug=slug)
            self.slug = slug
        super().save(*args, **kwargs)

    @staticmethod
    def generate_unique_slug(base_slug: str) -> str:
        slug = f'{base_slug}-{str(random.randint(0, 99))}'
        while Compilation.objects.filter(slug=slug).exists():
            slug = f'{base_slug}-{str(random.randint(0, 99))}'
        return slug
