from django.http import HttpResponse
from prometheus_client import CONTENT_TYPE_LATEST

from .resource_provider import rp


def get_metrics(request):
    prometheus_metrics = rp.get_metrics_use_case.run()

    return HttpResponse(content=prometheus_metrics, content_type=CONTENT_TYPE_LATEST)
