PROJECT_ROOT := ./gramotool

fmt:
	isort ${PROJECT_ROOT}

lint:
	flake8 --config ./config/flake8 ${PROJECT_ROOT}
	isort --check-only ${PROJECT_ROOT}

test:
	pytest --cov
